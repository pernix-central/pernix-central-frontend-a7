const express = require("express");
const path = require("path");

const app = express();

app.use(express.static(__dirname + "/dist/pc-fe-angular6"));

app.get("/*", function (_req, res) {
  res.sendFile(path.join(__dirname + "/dist/pc-fe-angular6/index.html"));
});

app.listen(process.env.PORT || 8080);
