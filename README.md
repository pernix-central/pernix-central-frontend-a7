# Pernix Central Front End - Angular 10

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.5 or higher.

##### NOTES:
- To run the project you need to install [NodeJS](https://serverspace.io/support/help/how-to-install-node-js-on-ubuntu-20-04/), version 18.16.0, as well as [NPM](https://docs.npmjs.com/about-npm), version 9.8.1. If using `nvm`, you can do
```
nvm install 18.16.0
nvm use 18.16.0
```

- If you already have `npm` installed but you're not on version 9.8.1, you can use npm itself to move to the required version: 
```
npm install -g npm@9.8.1
```
- You can also install Angular CLI version 17.1.1
```
npm install -g @angular/cli@17.1.1
```

## Install all dependencies
1. Use `npm install` or `npm i` to generate the `node_modules` folder with all necessary dependencies. If encountering issues, you
can try removing the `package-lock.json` as well, which will be recreated along with the `node_modules`.
2. If needed, use this command `npm audit fix` to fix the vulnerabilities.

### MAKE and PYTHON issues when installing dependencies in MacOS
If you encounter issues with your python version while using MacOS, you can try managing the version with `pyend`, like [this thread](https://github.com/nodejs/node-gyp/issues/2681) suggests: 

1. If you have `python` installed via `brew` - remove it `brew remove python`
2. Make sure to remove any aliases for `python` as well from your config file
3. Install `pyenv` via brew `brew install pyenv`
4. Install the desired version of `python` (for this workaround, version 3.10.13 works great)
5. Select the python version using `pyenv global 3.10.13`
6. Add the following snippet to your shell config file (`.zshrc`, `.zshenv`, etc...):
```
if command -v pyenv 1>/dev/null 2>&1; then
  eval "$(pyenv init -)"
fi
```
7. Restart the terminal - and then try runnning `npm i` again.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Errors during startup
- When running `ng serve`, if you get a compatibility error, try removing the `package-lock.json`, the `node_modules` and clearing npm's cache `npm cache clear --force`. Then, run `npm i` again and try running the app again. 

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--configuration production` flag for a production build.

# Testing
As of January 2024, this project does not have any working tests. The commands below need to be revised whenever a test framework is 
determined. `Protractor` has been removed due to deprecation, so an alternative e2e framework needs to be considered.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Deploying to staging

Steps:

1. Run `cd pernix-central-frontend-a7`
2. Run `git remote add heroku-staging https://git.heroku.com/pernix-central-ui-staging.git`
3. Run `git checkout develop`
4. Run `git pull`
5. Run `bin/deploy heroku-staging develop:master` (If command doesn't work, try `bash bin/deploy heroku-staging develop:master`)

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).