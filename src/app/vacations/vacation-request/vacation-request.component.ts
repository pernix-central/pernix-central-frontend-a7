import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { VacationsService } from '../vacations.service';
import { NotificationsService } from '@services/notifications.service';
import { SweetAlertService } from '@services/sweet-alert.service';

import { Employee } from '@models/employee';
import { Vacation } from '@models/vacation';
import { VacationRequest } from '@models/vacation-request';
import { environment } from '@environments/environment';
import { VacationRequestData } from '@interfaces/vacation-data';

import * as moment from 'moment';

const EMPLOYEE_KEY = environment.employee_key;

var formats = [
  moment.ISO_8601,
  "MM/DD/YYYY"
];

@Component({
  selector: 'app-vacation-request',
  templateUrl: './vacation-request.component.html'
})
export class VacationRequestComponent {
  currentUser: Employee = JSON.parse(localStorage.getItem(EMPLOYEE_KEY));
  vacationRequest: VacationRequest = new VacationRequest();
  availableDays: number = this.currentUser.employee_available_days;
  startDate: Date = new Date();
  endDate: Date = new Date();
  requestedDaysList: string[];
  isLoading: boolean;

  constructor(
    private vacationRequestService: VacationsService,
    private notificationsService: NotificationsService,
    private sweetAlertService: SweetAlertService,
    public dialogRef: MatDialogRef<VacationRequestComponent>,
    @Inject(MAT_DIALOG_DATA) public data: VacationRequestData
  ) {
    this.setAvailableDateRange();
    this.addVacation();
    this.requestedDaysList = data.requestedDaysList;
  }

  toggle() {
    this.isLoading = !this.isLoading;
  }

  setAvailableDateRange() {
    const todayDate: Date = new Date();
    this.startDate.setDate(todayDate.getDate());
    this.startDate.setHours(0, 0, 0, 0);
    this.endDate.setDate(todayDate.getDate() + 365);
    this.endDate.setHours(0, 0, 0, 0);
  }

  addVacation() {
    const newVacation = new Vacation();
    this.vacationRequest.vacation_dates_input_list.push(newVacation);
  }

  listenEnterEvent(event: KeyboardEvent, vacation: Vacation) {
    const target = event.target as HTMLInputElement;
    if (event.key == "Enter") {
      target.value = moment(vacation.date).format("MM/DD/YYYY");
    }
  }

  removeVacationDate(index: number, event: PointerEvent) {
    if (event.pointerId != -1) {
      this.vacationRequest.vacation_dates_input_list.splice(index, 1);
    }
  }

  getExactMonth(month: number): string {
    if (month == 0) {
      return '1';
    }
    return (month + 1).toString();
  }

  getDateFormat(date: Date): string {
    return this.getExactMonth(date.getMonth()) + '/' + date.getDate() + '/' + date.getFullYear();
  }

  getNotificationMessage(vacation: Vacation) {
    let date = new Date(vacation.date);
    let filterDates = this.vacationRequest.vacation_dates_input_list.filter((date) => date.date.toString() === vacation.date.toString());
    return (date.getDate().toString() == 'NaN') ? 'Please fill out the required fields' :
      !moment(date, formats, true).isValid() ? 'The correct format is MM/DD/YYYY' + ' in ' + this.getDateFormat(date) :
        this.requestedDaysList.includes(date.toString()) ? 'The date ' + this.getDateFormat(date) + ' has already been requested' :
          (date.getDay() == 6 || date.getDay() == 0) ? 'The date ' + this.getDateFormat(date) + ' is weekend' :
            (filterDates.length > 1) ? 'The date ' + this.getDateFormat(date) + ' is already included' :
              (date < this.startDate || date > this.endDate) ? this.getDateFormat(date) + ' is a invalid date' :
                '';
  }

  validateDates() {
    for (let vacation of this.vacationRequest.vacation_dates_input_list) {
      let message = this.getNotificationMessage(vacation);
      if (message != '') {
        this.notificationsService.showNotification(message, 'danger');
        return;
      }
    }
    if (this.vacationRequest.vacation_dates_input_list.length > this.availableDays) {
      this.sweetAlertService.showConfirmationDialog().fire({
        html: '<b>Are you sure you want to continue?</b><br><br>Your requested days exceed your available vacation days',
      }).then((result) => {
        if (result.isConfirmed) {
          this.sendVacationRequest();
        }
      })
    } else {
      this.sendVacationRequest();
    }
  }

  showVacationInformation(): void {
    this.sweetAlertService.showInformationDialog().fire({
      html: '<b>Sus vacaciones se aprobarán dentro de los siguientes 3 días hábiles.</b>' +
        '<br><br>Póngase en contacto con un administrador si se trata de un asunto urgente.',
    })
  }

  sortVacationDates(vacations: Vacation[]) {
    const data = vacations.slice();
    return data.sort((a, b) => { return (Date.parse(a.date) - Date.parse(b.date)) });
  }

  sendVacationRequest() {
    this.toggle();
    this.vacationRequest.employee_id = this.currentUser.id;
    this.vacationRequest.requested_date = new Date().toISOString();
    this.vacationRequest.employee_name = this.currentUser.first_name;
    this.vacationRequest.vacation_dates_input_list = this.sortVacationDates(this.vacationRequest.vacation_dates_input_list);
    this.vacationRequest.client_approval = this.vacationRequest.client_approval.replace(/[,\n]+/g, ' ');
    this.vacationRequestService.createVacationRequest(this.vacationRequest).subscribe((data: any) => {
      if (data.employee_id === this.vacationRequest.employee_id) {
        this.notificationsService.showNotification('Vacation day added successfully', 'success');
        this.toggle();
        this.dialogRef.close(true);
        this.showVacationInformation();
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
        this.dialogRef.close(true);
      }
    })
  }

  cancel() {
    if (!!(this.vacationRequest.vacation_dates_input_list[0].date) || !!(this.vacationRequest.client_approval)) {
      this.sweetAlertService.showConfirmationDialog().fire({
        html: '<b>Are you sure you want to exit?<b><br><br>Any changes made will be lost!',
      }).then((result) => {
        if (result.isConfirmed) {
          this.notificationsService.showNotification('Vacation request canceled', 'warning');
          this.dialogRef.close();
        } else {
          result.dismiss === this.sweetAlertService.showConfirmationDialog().DismissReason.cancel
        }
      })
    } else {
      this.notificationsService.showNotification('Vacation request canceled', 'warning');
      this.dialogRef.close();
    }
  }
}