import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '@services/storage.service';

import { VacationRequest } from '@models/vacation-request';
import { environment } from '@environments/environment';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class VacationsService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  getAllUserVacations(employee_id: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/vacation_request/user_vacations?employeeid=${employee_id}`;
    return this.http.get<VacationRequest[]>(url, this.httpOptions);
  }

  getAllVacations() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/vacation_request/index`;

    return this.http.get<VacationRequest[]>(url, this.httpOptions);
  }

  updateVacationStatus(vacation_request: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/vacation_request/update`;
    return this.http.put(url, vacation_request, this.httpOptions);
  }

  createVacationRequest(vacation_request: VacationRequest) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/vacation_request/create`;
    return this.http.post(url, vacation_request, this.httpOptions);
  }

  deleteVacationRequest(id: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/vacation_request/${id}`
    return this.http.delete(url, this.httpOptions)
  }

  acceptOrDenyVacation(params: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/vacation_request/${params['request_id']}/update_status`;
    return this.http.put(url, params['options'] || {}, this.httpOptions);
  }
}
