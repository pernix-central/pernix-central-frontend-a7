import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { ComponentsModule } from '@components/components.module';

import { VacationsComponent } from './vacations.component';
import { VacationsHistoryComponent } from './vacations-history/vacations-history.component';
import { VacationRequestComponent } from './vacation-request/vacation-request.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    MatFormFieldModule,
    MatInputModule
  ],
  declarations: [
    VacationsComponent,
    VacationsHistoryComponent,
    VacationRequestComponent
  ],
  exports: [
    MatFormFieldModule,
    MatInputModule,
  ],
  providers: [],
  bootstrap: [VacationsComponent]
})
export class VacationsModule { }
