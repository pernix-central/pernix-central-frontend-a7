import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Location } from '@angular/common';

import { VacationsService } from '../vacations.service';
import { PermissionsService } from '@services/permissions.service';
import { EmployeesService } from 'src/app/employees/employees.service';
import { NotificationsService } from '@services/notifications.service';

import { Vacation } from '@models/vacation';
import { VacationRequest } from '@models/vacation-request';
import { Employee } from '@models/employee';
import { RequestStatus } from '@enums/request-status';
import { environment } from '@environments/environment';

import { VacationDetailsComponent } from '../vacation-details/vacation-details.component';
import { VacationRequestComponent } from '../vacation-request/vacation-request.component';

const EMPLOYEE_KEY = environment.employee_key;

@Component({
  selector: 'app-vacations-history',
  templateUrl: './vacations-history.component.html'
})
export class VacationsHistoryComponent implements OnInit {
  vacationList: VacationRequest[];
  adminVacationList: VacationRequest[];
  vacationRequestListData: VacationRequest[];
  dates: Vacation[];
  employeeList: Employee[];
  employeeListData: Employee[];
  currentEmployee: Employee;
  title: string = "Vacation History";
  reviewerName: string;
  status: string;
  employeesHistory: boolean = false;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private permissionsService: PermissionsService,
    private vacationService: VacationsService,
    private employeesService: EmployeesService,
    private route: ActivatedRoute,
    private location: Location,
    private notificationsService: NotificationsService,
  ) {
    this.currentEmployee = JSON.parse(localStorage.getItem(EMPLOYEE_KEY));
  }

  vacationDetails(vacationId: number) {
    let vacationRequestData = this.vacationList.find(vacation => vacation.id == vacationId);
    if (!!(vacationRequestData)) {
      const dialogRef = this.dialog.open(VacationDetailsComponent, {
        width: '45%',
        data: {
          vacationRequestData: this.vacationList.find(vacation => vacation.id == vacationId)
        },
        disableClose: true,
        panelClass: 'mobile-dialog',
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.getVacationsByUser(vacationRequestData.employee_id, 'Vacation History');
        }
      });
    } else {
      this.getVacationHistory();
    }
  }

  currentEmployeeIsAdmin() {
    return this.permissionsService.currentEmployeeIsAdmin();
  }

  isCrafter() {
    return this.permissionsService.currentEmployeeIsCrafter();
  }

  private getVacationRequestList(): string[] {
    let requestedDaysList: Date[] = [];
    let vacationRequestList: VacationRequest[]
    if (!!(this.adminVacationList)) {
      vacationRequestList = this.adminVacationList;
    } else {
      vacationRequestList = this.vacationList;
    }
    vacationRequestList.forEach((vacationRequest) => {
      if (vacationRequest.status == RequestStatus.Accepted || vacationRequest.status == RequestStatus.Pending) {
        vacationRequest.vacation_dates_output_list.forEach((requestedDate) => {
          requestedDaysList.push(new Date(requestedDate.date));
        });
      }
    });

    return requestedDaysList.toString().split(',');
  }

  goToVacationRequest() {
    const dialogRef = this.dialog.open(VacationRequestComponent, {
      width: '70%',
      data: {
        requestedDaysList: this.getVacationRequestList()
      },
      disableClose: true,
      panelClass: 'mobile-dialog',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getVacationHistory();
      }
    });
  }

  getEmployee() {
    let employee: Employee;
    this.route.data.subscribe((data: { employee: Employee }) => {
      employee = data.employee;
    })

    return employee;
  }

  downloadCSV(employeeId: number) {
    this.vacationService.getAllUserVacations(employeeId).subscribe((data: any) => {
      this.downloadFile(data, ["employee_name", "requested_date", "status", "reviewer_name", "review_date", "client_approval", "vacation_dates_output_list"]);
    });
  }

  private downloadFile(data: VacationRequest[], columnHeaders: string[]) {
    let csvData = this.convertToCSV(data, columnHeaders);
    let blob = new Blob(['﻿' + csvData], {
      type: 'text/csv;charset=utf-8;',
    });
    let download = document.createElement('a');
    let url = URL.createObjectURL(blob);
    let isSafariBrowser =
      navigator.userAgent.indexOf('Safari') != -1 &&
      navigator.userAgent.indexOf('Chrome') == -1;
    if (isSafariBrowser) {
      download.setAttribute('target', '_blank');
    }
    download.setAttribute('href', url);
    download.setAttribute('download', 'historial_de_vacaciones.csv');
    download.style.visibility = 'hidden';
    document.body.appendChild(download);
    download.click();
    document.body.removeChild(download);
  }

  private convertToCSV(vacationHistory: VacationRequest[], headerList: string[]) {
    let document = '';
    let header = 'Nombre del empleado,' + 'Día de la solicitud,' + 'Estado,' + 'Nombre del revisor,' + 'Fecha de la revisión,' + 'Aprobación del cliente,' + 'Días solicitados';
    document += header + '\r\n';
    for (let i = 0; i < vacationHistory.length; i++) {
      let row = '';
      for (let index in headerList) {
        let head = headerList[index];
        if (head === 'vacation_dates_output_list') {
          for (let a = 0; a < vacationHistory[i][head].length; a++) {
            row += vacationHistory[i][head][a].date + ',';
          }
        } else {
          if (!(vacationHistory[i][head])) {
            row += 'N/A' + ',';
          } else {
            row += vacationHistory[i][head] + ',';
          }
        }
      }
      document += row + '\r\n';
    }
    return document;
  }

  getVacationsByUser(employeeId: number, title: string) {
    this.vacationRequestListData = null;
    this.vacationService.getAllUserVacations(employeeId).subscribe((data: any) => {
      this.setVacationsValues(data, title);
      this.openDialogFromURL();
    });
  }

  private setVacationsValues(data: any, title: string,) {
    this.vacationList = this.sortData(data);
    this.vacationRequestListData = this.vacationList;
    this.title = title;
  }

  private fillVacationList(employee: Employee) {
    if (!employee) {
      this.getVacationsByUser(this.currentEmployee.id, `Vacation History`);
    } else {
      if (employee.id == this.currentEmployee.id) {
        this.getVacationsByUser(this.currentEmployee.id, `My Vacation History`);
      } else {
        this.getVacationsByUser(employee.id, `Vacation History of ${employee.first_name}`);
      }
    }
  }

  private sortData(vacations: VacationRequest[]) {
    const data = vacations.slice();
    return data.sort((a, b) => { return (Date.parse(b.requested_date) - Date.parse(a.requested_date)) });
  }

  filter(searchTarget: string) {
    this.vacationList = (searchTarget.length >= 3) ?
      this.filterListByReviewerAndStatus(searchTarget, this.vacationRequestListData) :
      this.vacationRequestListData;
  }

  filterByEmployeeName(searchTarget: string) {
    this.employeeList = (searchTarget.length >= 3) ?
      this.filterListByEmployeeName(searchTarget, this.employeeListData) :
      this.employeeListData;
  }

  private filterListByEmployeeName(searchTarget: string, dataList: Employee[]) {
    return dataList.filter((employee: Employee) => {
      return employee.first_name.toLowerCase().includes(searchTarget.toLowerCase()) ||
        employee.last_name.toLowerCase().includes(searchTarget.toLowerCase());
    });
  }

  private filterListByReviewerAndStatus(searchTarget: string, dataList: VacationRequest[]) {
    return dataList.filter((vacationRequest: VacationRequest) => {
      if (vacationRequest.reviewer_name) {
        return vacationRequest.reviewer_name.toLowerCase().includes(searchTarget.toLowerCase()) ||
          vacationRequest.status.toLowerCase().includes(searchTarget.toLowerCase());
      } else {
        return vacationRequest.status.toLowerCase().includes(searchTarget.toLowerCase());
      }
    });
  }

  getVacationHistory() {
    let employee: Employee = this.getEmployee();
    this.fillVacationList(employee);
  }

  openDialogFromURL() {
    if (!!(this.route.snapshot.paramMap.get('vacation'))) {
      this.vacationDetails((this.route.snapshot.paramMap.get('vacation') as unknown) as number);
    }
  }

  compareUrl() {
    this.router.events.subscribe((data) => {
      if (data instanceof NavigationEnd) {
        this.openDialogFromURL();
      }
    })
  }

  private filterByActiveAndByVacationControl(isActive: boolean, employees: Employee[]) {
    return employees.filter((employee) => employee.active == isActive && employee.vacation_control);
  }

  async acceptOrDenyVacations(): Promise<void> {
    const currentRoute = this.route.snapshot;
    const params = {
      request_id: currentRoute.queryParams['request_id'],
      status: currentRoute.queryParams['status'],
    };

    if (params.request_id && params.status) {
      try {
        const options = {
          status: currentRoute.queryParams['status'],
          reviewer_id: this.currentEmployee.id,
        }
        const data: any = await this.vacationService.acceptOrDenyVacation({ ...params, options }).toPromise();

        if (data.error) {
          this.notificationsService.showNotification(data.error, 'danger');
        } else {
          this.notificationsService.showNotification(data.message, 'success');
        }
      } catch (error) {
        console.error(error);
      }
    }
  }

  async ngOnInit(): Promise<void> {
    try {
      if (this.currentEmployeeIsAdmin()) {
        await this.acceptOrDenyVacations();
      }

      if (this.currentEmployeeIsAdmin() && this.location.path().includes('vacations/history')) {
        this.employeesHistory = true;

        // Fetch employees
        const employeesData: any = await this.employeesService.getEmployees().toPromise();
        this.employeeList = this.filterByActiveAndByVacationControl(true, employeesData);
        this.employeeListData = this.employeeList;

        // Fetch user vacations
        const userVacationsData: any = await this.vacationService.getAllUserVacations(this.currentEmployee.id).toPromise();
        this.adminVacationList = userVacationsData;
      } else if (this.location.path().includes('vacation_history') || !this.currentEmployeeIsAdmin()) {
        this.getVacationHistory();
      }

      this.compareUrl();
    } catch (error) {
      console.error(error);
    }
  }
}
