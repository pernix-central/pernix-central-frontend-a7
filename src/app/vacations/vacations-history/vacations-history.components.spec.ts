import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VacationsHistoryComponent } from './vacations-history.component';

describe('VacationsHistoryComponent', () => {
  let component: VacationsHistoryComponent;
  let fixture: ComponentFixture<VacationsHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [VacationsHistoryComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VacationsHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
