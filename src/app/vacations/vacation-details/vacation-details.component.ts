import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { VacationsService } from '../vacations.service';
import { EmployeesService } from '../../employees/employees.service';
import { PermissionsService } from '@services/permissions.service';
import { NotificationsService } from '@services/notifications.service';
import { SweetAlertService } from '@services/sweet-alert.service';

import { Status } from '@enums/status';
import { RequestStatus } from '@enums/request-status';

import { Employee } from '@models/employee';
import { VacationRequest } from '@models/vacation-request';
import { environment } from '@environments/environment';
import { VacationRequestData } from '@interfaces/vacation-data';

const EMPLOYEE_KEY = environment.employee_key;

@Component({
  selector: "vacation-details",
  templateUrl: "./vacation-details.component.html"
})
export class VacationDetailsComponent {
  vacation_request: VacationRequest;
  currentUserIsAdmin: boolean;
  statusIsPending: boolean;
  statusIsAccepted: boolean;
  currentEmployee: Employee = JSON.parse(localStorage.getItem(EMPLOYEE_KEY));
  vacationRequestStatus: string;
  employee: Employee;
  vacationDays: number;
  requestStatus = RequestStatus;
  isLoading: boolean;

  constructor(
    private permissionsService: PermissionsService,
    private vacationService: VacationsService,
    private employeeService: EmployeesService,
    private notificationsService: NotificationsService,
    private sweetAlertService: SweetAlertService,
    private router: Router,
    public dialogRef: MatDialogRef<VacationDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: VacationRequestData
  ) {
    this.vacation_request = data.vacationRequestData;
    this.vacationRequestStatus = this.vacation_request.status;
    this.vacationDays = this.vacation_request.vacation_dates_output_list.length;
    this.currentUserIsAdmin = this.permissionsService.currentEmployeeIsAdmin();
    this.statusIsPending = this.isPending();
    this.statusIsAccepted = this.isAccepted();
    this.getEmployee();
  }

  save() {
    if (this.vacation_request.status != this.vacationRequestStatus) {
      this.sweetAlertService.showConfirmationDialog().fire({
        html: `<b>Are you sure you want to "${this.vacationRequestStatus}" vacation days?</b>
        <br><br>This action CANNOT be undone!`,
      }).then((result) => {
        if (result.isConfirmed) {
          this.toggle();
          this.updateVacationRequest();
          this.vacationService.updateVacationStatus(this.vacation_request).subscribe((data: any) => {
            this.vacation_request = data;
            this.statusIsPending = false;
            this.vacationRequestStatus = this.vacation_request.status;
            this.notificationsService.showNotification('Your changes have been saved', 'success');
            this.dialogRef.close(true);
          }, (_error: any) => {
            this.vacation_request.reviewer_name = null;
            this.vacationRequestStatus = this.vacation_request.status;
            this.toggle();
          },
          );
          this.clearPath();
        }
      })
    } else {
      this.dialogRef.close();
      this.clearPath();
    }
  }

  close() {
    if (this.vacation_request.status != this.vacationRequestStatus) {
      this.sweetAlertService.showConfirmationDialog().fire({
        html: '<b>Are you sure you want to exit?</b><br><br>Any changes made will be lost!',
      }).then((result) => {
        if (result.isConfirmed) {
          this.vacationRequestStatus = this.vacation_request.status;
          this.dialogRef.close();
          this.clearPath();
        }
      })
    } else {
      this.dialogRef.close();
      this.clearPath();
    }
  }

  showStatus() {
    return !this.currentUserIsAdmin || this.vacation_request.status == RequestStatus.Denied || this.vacation_request.status == RequestStatus.Cancelled
  }

  toggle() {
    this.isLoading = !this.isLoading;
  }

  private clearPath(): void {
    let result = this.router.url.split(";")
    this.router.navigateByUrl(result[0]);
  }

  getEmployee() {
    this.employeeService.getEmployee(this.vacation_request.employee_id).
      subscribe((data: any) => { this.employee = data; });
  }

  canCheckEmployeeName() {
    return this.currentUserIsAdmin || this.employeeIsCurrentEmployee();
  }

  employeeIsCurrentEmployee() {
    return (this.currentEmployee.id != this.vacation_request.employee_id);
  }

  updateVacationRequest() {
    this.vacation_request.reviewer_id = this.currentEmployee.id;
    this.vacation_request.review_date = new Date().toISOString();
    if (this.vacationRequestStatus == RequestStatus.Accept) {
      this.vacation_request.status = RequestStatus.Accepted;
    } else if (this.vacationRequestStatus == RequestStatus.Deny) {
      this.vacation_request.status = RequestStatus.Denied;
    } else {
      this.vacation_request.status = RequestStatus.Cancelled;
    }
  }

  isPending() {
    return this.vacation_request.status == RequestStatus.Pending;
  }

  isAccepted() {
    return this.vacation_request.status == RequestStatus.Accepted;
  }

  statusAllowedSave() {
    return this.isPending() || this.isAccepted();
  }

  deleteVacationRequest(event: any, id: number) {
    event.stopPropagation();

    this.sweetAlertService.showConfirmationDialog().fire({
      html: '<b>Are you sure you want to remove this vacation request?</b>',
    }).then((result) => {
      if (result.isConfirmed) {
        this.toggle();
        this.vacationService.deleteVacationRequest(id).subscribe((data: any) => {
          if (data == Status.OK) {
            this.dialogRef.close(true);
            this.clearPath();
            this.notificationsService.showNotification('Vacation request deleted successfully', 'success');
          } else {
            this.toggle();
            this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
          }
        });
      }
    })
  }
}
