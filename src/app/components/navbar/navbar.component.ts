import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialog, MatDialogRef } from '@angular/material/dialog';

import { StorageService } from '@services/storage.service';
import { NotificationMessagesService } from '../../notification-messages/notification-messages.service';
import { NavbarService } from '@services/navbar.service';

import { Employee } from '@models/employee';
import { NotificationMessage } from '@models/notification-message';
import { NotificationTypes } from '@enums/notification-types';
import { environment } from '@environments/environment';

import { NotificationTrayComponent } from '../../notification-messages/notification-tray/notification-tray.component';

import { CelebrationDialogComponent } from '@dialogs/celebration-dialog/celebration-dialog.component';
import { Subscription, interval } from 'rxjs';
import { switchMap } from 'rxjs/operators';

const NAV_TITLE_KEY = environment.nav_title_key;
const EMPLOYEE_KEY = environment.employee_key;

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html'
})
export class NavbarComponent implements OnInit {
  private apiUrl: string;
  private storage: Storage;
  employee: Employee;
  unreadNotificationCount: number;
  notificationMessages: NotificationMessage[] = [];
  private newNotificationPollSubscription: Subscription;
  private newNotificationPollInterval: number = 30000
  private notificationDialogRef: MatDialogRef<NotificationTrayComponent>;

  constructor(
    private storageService: StorageService,
    private notificationMessagesService: NotificationMessagesService,
    private navbarService: NavbarService,
    private router: Router,
    private dialog: MatDialog
  ) {
    this.storage = this.storageService.get();
    this.apiUrl = environment.apiUrl;
  }

  initNewNotificationPollSubscription() {
    this.newNotificationPollSubscription = interval(this.newNotificationPollInterval)
      .pipe(
        switchMap(() => this.notificationMessagesService.getNotificationMessages())
      )
      .subscribe((data: any) => {
        this.notificationMessages = data;
        this.unreadNotificationCount = this.notificationMessages.filter(message => !message.read).length
        if (this.notificationDialogRef) {
          this.notificationDialogRef.componentInstance.refreshNotificationList(data);
        }
      })
  }

  getTitle() {
    return this.storage.getItem(NAV_TITLE_KEY);
  }

  getUserName() {
    return this.employee.first_name + " " + this.employee.last_name;
  }

  isLogin(): boolean {
    return this.router.url == '/login';
  }

  hasImage() {
    return !!this.employee.profile;
  }

  getNotifications() {
    this.notificationMessagesService.getNotificationMessages().subscribe((data: any) => {
      this.notificationMessages = data;
      this.unreadNotificationCount = this.notificationMessages.filter(data => data.read == false).length;
    })
  }

  celebrationList(title: string, text: string, extra_information: string): void {
    this.dialog.open(CelebrationDialogComponent, {
      width: '40%',
      data: {
        title: title,
        text: text,
        celebrationData: extra_information
      },
    })
  }

  openNotificationTray() {
    this.notificationMessagesService.getNotificationMessages().subscribe((notification) => {

      this.notificationDialogRef = this.dialog.open(NotificationTrayComponent, {
        width: '40%',
        position: {
          right: '25px',
          top: '70px'
        },
        backdropClass: 'backdrop-opacity-0',
        data: {
          notificationMessageData: notification
        },
        panelClass: 'notification-tray',
      });
      this.notificationDialogRef.afterClosed().subscribe(result => {
        if (!!(result)) {
          if (result.readAll) {
            this.unreadNotificationCount = 0;
          }
          if (result.readNotification) {
            this.unreadNotificationCount--;
          }
          switch (result.notificationType) {
            case NotificationTypes.VACATIONSUBMISSION:
              this.router.navigate(['../../../', 'vacations', result.senderId, 'vacation_history', { vacation: result.notifyableId }]);
              break;
            case NotificationTypes.ROLECHANGE:
              this.router.navigate(['../../../', 'my-profile', this.employee.id, 'details']);
              break;
            case NotificationTypes.VACATIONREVISION:
              this.router.navigate(['../../../', 'vacations', this.employee.id, 'vacation_history', { vacation: result.notifyableId }]);
              break;
            case NotificationTypes.ADMINVACATIONREVISION:
              this.router.navigate(['../../../', 'vacations', result.senderId, 'vacation_history', { vacation: result.notifyableId }]);
              break;
            case NotificationTypes.VACATIONREMINDER:
              this.router.navigate(['../../../', 'vacations', 'history']);
              break;
            case NotificationTypes.SUGGESTIONRESPONSES:
              this.router.navigate(['../../../', 'suggestions', result.extraInfo, 'details']);
              break;
            case NotificationTypes.MYBIRTHDAY:
              this.celebrationList("Happy Birthday!", "", result.extraInfo);
              break;
            case NotificationTypes.BIRTHDAY:
              this.celebrationList("Birthdays Today", "From all here at Pernix we wish a Happy Birthday to:", result.extraInfo);
              break;
            case NotificationTypes.ANNIVERSARY:
              this.celebrationList("Anniversaries Today", "From all here at Pernix we wish a Happy Anniversary to:", result.extraInfo);
              break;
            case NotificationTypes.SCHEDULECHANGE:
              this.router.navigate(['../../../', 'employees', result.senderId, 'details']);
              break;
            case NotificationTypes.POINTREDEMPTIONREQUESTSUBMISSION:
              this.router.navigate(['../../../', 'wallet', result.senderId, 'wallet_history', { point_request: result.notifyableId }]);
              break;
            case NotificationTypes.POINTREQUESTREVISION:
              this.router.navigate(['../../../', 'wallet', this.employee.id, 'wallet_history', { point_request: result.notifyableId }]);
              break;
            case NotificationTypes.POINTREDEMPTIONREQUESTREVISION:
              this.router.navigate(['../../../', 'wallet', this.employee.id, 'wallet_history', { point_request: result.notifyableId }]);
              break;
            case NotificationTypes.POINTREQUESTREMINDER:
              this.router.navigate(['../../../', 'wallet', 'summary']);
              break;
            case NotificationTypes.ADMINPOINTREQUESTREVISION:
              this.router.navigate(['../../../', 'wallet', result.senderId, 'wallet_history', { point_request: result.notifyableId }]);
              break;
            default:
              break;
          }
        }
        this.notificationDialogRef = undefined;
      });
    })
  }

  ngOnInit() {
    this.employee = JSON.parse(this.storage.getItem(EMPLOYEE_KEY));
    if (this.employee != null) {
      this.getNotifications();
      this.initNewNotificationPollSubscription();

      this.navbarService.getUpdate$().subscribe(update => {
        if (update) {
          this.employee = JSON.parse(this.storage.getItem(EMPLOYEE_KEY));
        }
      })
    }
  }

  ngOnDestroy() {
    if (this.newNotificationPollSubscription) {
      this.newNotificationPollSubscription.unsubscribe();
    }
  }
}
