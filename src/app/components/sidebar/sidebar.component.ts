import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { PermissionsService } from '@services/permissions.service';
import { StorageService } from '@services/storage.service';
import { SweetAlertService } from '@services/sweet-alert.service';

import { Employee } from '@models/employee';
import { environment } from '@environments/environment';

import { Output, EventEmitter } from '@angular/core';

import { Store } from '@ngxs/store';
import { Logout } from '../.././login/login.action';

declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}

export const ROUTES: RouteInfo[] = [
  { path: '/home', title: 'Checklist', icon: 'checklist', class: '' },
  { path: '/employees', title: 'Employees', icon: 'groups', class: '' },
  { path: '/suggestions', title: 'Suggestion box', icon: 'feedback', class: '' },
  { path: '/vacations', title: 'Vacations', icon: 'flight', class: '' },
  { path: '/wallet', title: 'Wallet', icon: 'monetization_on', class: '' },
  { path: '/official-links', title: 'Official Links', icon: 'link', class: '' },
];

const EMPLOYEE_KEY = environment.employee_key;
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  private storage: Storage;
  employee: Employee;
  @Output() collapseEvent = new EventEmitter<boolean>();
  openedSidebar: boolean;

  constructor(
    private storageService: StorageService,
    private router: Router,
    private permissionsService: PermissionsService,
    private store: Store,
    private sweetAlertService: SweetAlertService,
  ) {
    this.storage = this.storageService.get();
    this.openedSidebar = true;
  }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.employee = JSON.parse(this.storage.getItem(EMPLOYEE_KEY));
  }

  toggleSidebar() {
    this.openedSidebar = !this.openedSidebar;
    this.collapseEvent.emit(this.openedSidebar);
  }

  logOut() {
    this.sweetAlertService.showConfirmationDialog().fire({
      html: '<b>Are you sure you want to log out?</b>',
    }).then((result) => {
      if (result.isConfirmed) {
        this.store.dispatch(new Logout());
        this.toggleSidebar();
        this.router.navigate(['login']);
      } else if (
        result.dismiss === this.sweetAlertService.showConfirmationDialog().DismissReason.cancel
      ) {
      }
    })

  }

  canAccessModules(title: string) {
    if (title === 'Vacations') return !(this.permissionsService.currentEmployeeIsApprentice()) && this.employee.vacation_control || this.permissionsService.currentEmployeeIsAdmin();
    return true;
  }

}
