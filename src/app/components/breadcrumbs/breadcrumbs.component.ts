import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

import { Employee } from '@models/employee';
import { environment } from '@environments/environment';

const EMPLOYEE_KEY = environment.employee_key;

@Component({
  selector: 'app-breadcrumbs',
  templateUrl: './breadcrumbs.component.html'
})
export class BreadcrumbsComponent implements OnInit {
  currentEmployee: Employee = JSON.parse(localStorage.getItem(EMPLOYEE_KEY));
  pathArray: string[];
  employeeId: number;

  constructor(
    private location: Location
  ) { }

  getPathNames(array: string[]) {
    let result: string[] = [];
    array.forEach(element => {
      if (!!(element)) {
        if (!isNaN(+element)) {
          this.employeeId = +element;
          if (this.location.path().includes('my-profile')) {
            result.push('my profile');
          } else {
            result.push('details');
          }
        } else if (element.includes('summary')) {
          result.push(element.split('?')[0]);
        } else if ((element !== 'details') && (element !== 'my-profile')) {
          result.push(element.split(';')[0]);
        }
      }
    });
    return result;
  }

  getPath(pathIndex: number): (string | number)[] {
    if (this.pathArray[pathIndex] === 'details' || this.pathArray[pathIndex] === 'my profile') {
      return ['../../', this.employeeId, 'details'];
    } else if (this.pathArray[pathIndex + 1] === 'new' || this.pathArray[pathIndex] === 'wallet') {
      return ['../../', this.pathArray[pathIndex]];
    } else {
      return ['../../../', this.pathArray[pathIndex]];
    }
  }

  ngOnInit(): void {
    this.pathArray = this.getPathNames(this.location.path().split('/'));
  }

}
