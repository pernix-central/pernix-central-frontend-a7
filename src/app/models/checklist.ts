export class Checklist {
  id: number;
  task_name: string;
  task_type: string = 'custom';
  is_global: boolean = false;
  is_editable: boolean = false;
  is_new: boolean = true;
  is_completed: boolean = false;
}