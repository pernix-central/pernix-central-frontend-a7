import { Schedule } from './schedule';
import { Role } from './role';

export class Employee {
  id?: number;
  name: string;
  first_name: string;
  last_name: string;
  birthdate: string;
  email: string;
  password: string;
  password_confirmation: string;
  has_car: boolean;
  blog_url: string;
  computer_serial_number: string;
  active: boolean;
  mac_address?: any;
  hire_date: string;
  on_vacation: boolean;
  can_be_mentor: boolean;
  vacation_control: boolean;
  role: Role;
  schedule: Schedule;
  profile: string;
  attachment: File;
  skill_names?: string[];
  phone_number: string;
  id_number: string;
  approved_days: number;
  employee_available_days: number;
  employee_seniority_days: number;
  seniority_days: number;
  employee_enjoyed_days: number;
  employee_total_days: number;
  has_pending_requests: boolean;
  my_way_points_available: number;
}
