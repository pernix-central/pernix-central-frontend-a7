export class NotificationSettings {
  id?: number;
  anniversary: { email: boolean, tray: boolean };
  birthday: { email: boolean, tray: boolean };
  change_role: { email: boolean, tray: boolean };
  vacation_request_reviewed: { email: boolean, tray: boolean };
  vacation_request_submitted: { email: boolean, tray: boolean };
  vacation_request_reminder: { 
    email: boolean,
    tray: boolean,
    notify_on: string[] | undefined
  };
  wallet_request_reminder: {
    email: boolean, 
    tray: boolean, 
    notify_on: string[] | undefined
  };
  wallet_request_reviewed: { email: boolean, tray: boolean };
  suggestion_responses: { email: boolean, tray: boolean };
  change_schedule: { email: boolean, tray: boolean };
  request_removal: { email: boolean, tray: boolean};
};
