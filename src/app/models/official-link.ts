export class OfficialLink {
    id: number;
    name: string;
    hyperlink: string;
}