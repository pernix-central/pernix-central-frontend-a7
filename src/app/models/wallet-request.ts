import { RequestStatus } from "@enums/request-status";

export class WalletRequest {
    id?: number;
    created_at: Date;
    request_name: string;
    request_points: number;
    request_type: string;
    request_comments?: string;
    status: RequestStatus;
    employee_id: number;
    employee_name: string;
    reviewer_id: number;
    reviewer_name: string;
    review_date: string;
    formatted_created_at: string;
    is_marked: boolean;
}