export class NotificationMessage {
  id?: number;
  read: boolean;
  notification_type: string;
  extra_information: string;
  sender_id: number;
  notifyable_id :number;
};
