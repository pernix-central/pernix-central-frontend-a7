export class Award {
    id?: number;
    name: string;
    points: number;
}