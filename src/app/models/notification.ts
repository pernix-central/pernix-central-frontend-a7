export class Notification {
  id?: number;
  notification_type: string;
  title: string;
  content: string;
  active: boolean;
  _destroy: boolean;
};
