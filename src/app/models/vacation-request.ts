import { Vacation } from "./vacation";

export class VacationRequest {
    id?: number;
    requested_date: string;
    client_approval: string = "";
    status: string;
    employee_name: string;
    employee_id: number;
    reviewer_name: string;
    reviewer_id: number;
    review_date: string;
    vacation_dates_output_list: Vacation[] = [];
    vacation_dates_input_list: Vacation[] = [];
}