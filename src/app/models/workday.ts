import { Period } from './period';

export class Workday {
  id?: number;
  week_day: string;
  wfh: boolean;
  periods: Period[];
}
