import { Injectable, inject } from '@angular/core';
import { Router, ActivatedRouteSnapshot, RouterStateSnapshot, CanActivateFn, RouterState } from '@angular/router';

import { PermissionsService } from '@services/permissions.service';
import { NotificationsService } from '@services/notifications.service';
import { StorageService } from '@services/storage.service';


export const RoleGuard: CanActivateFn = (
  route: ActivatedRouteSnapshot,
  state: RouterStateSnapshot
) => {
  const permissionsService:PermissionsService = inject(PermissionsService);
  const notificationsService: NotificationsService = inject(NotificationsService);
  const router:Router = inject(Router);
  const storageService:StorageService = inject(StorageService)


  const storage:Storage = storageService.get();

  const expectedEmployeeId = +route.paramMap.get('employee_id')
  
  let response: boolean;
  let isAdminUser = permissionsService.currentEmployeeIsAdmin();
  let isApprenticeUser = permissionsService.currentEmployeeIsApprentice();
  let isCurrentEmployee = permissionsService.isCurrentEmployeeId(expectedEmployeeId);
  let vacationControlIsActive = permissionsService.vacationControlIsActive();

  if (state.url.includes('employees')) {
    if (state.url.includes('edit')) {
      response = isAdminUser || isCurrentEmployee;
    } else if (state.url.includes('new')) {
      response = isAdminUser;
    }
  } else if (state.url.includes('my-profile')) {
    if (state.url.includes('edit')) {
      response = isAdminUser || isCurrentEmployee;
    }
  } else if (state.url.includes('vacations')) {
    if (state.url.includes('vacation_history')) {
      if (isCurrentEmployee) {
        if (vacationControlIsActive) {
          response = true;
        } else {
          response = false;
        }
      } else {
        response = isAdminUser;
      }
    } else if (state.url.includes('history')) {
      response = !isApprenticeUser && vacationControlIsActive || isAdminUser;
    }
  } else if (state.url.includes('wallet')) {
    if (state.url.includes('wallet_history')) {
      response = isCurrentEmployee ? true : isAdminUser;
    } else if (state.url.includes('summary')) {
      response = isAdminUser;
    }
  }

  if (!response) {
    notificationsService.showNotification('Unauthorized!', 'danger');
    storage.clear();
    router.navigate(['login']);
    return response;
  } else {
    return response;
  }
}
