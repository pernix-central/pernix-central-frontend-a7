import { inject } from '@angular/core';
import { CanActivateFn, Router } from '@angular/router';
import { Location } from '@angular/common';

import { LoginService } from '../login/login.service';
import { StorageService } from '@services/storage.service';
import { NotificationsService } from '@services/notifications.service';
import { EmployeesService } from '../employees/employees.service';
import { NavbarService } from '@services/navbar.service';

import { environment } from '@environments/environment';

export const AuthGuard: CanActivateFn = () => {
  const EMPLOYEE_KEY = environment.employee_key;
  const loginService: LoginService = inject(LoginService);
  const storageService: StorageService = inject(StorageService);
  const router: Router = inject(Router);
  const location: Location = inject(Location);
  const notificationsService: NotificationsService = inject(NotificationsService);
  const employeeService: EmployeesService = inject(EmployeesService);
  const navbarService: NavbarService = inject(NavbarService);
  const storage: Storage = storageService.get();

  if (!loginService.isAuthenticated()) {
    if (!location.path().includes('login')) {
      storage.setItem('URL', location.path());
    }
    notificationsService.showNotification('Unauthorized. You need to log in first!', 'danger');
    router.navigate(['login']);
    return false;
  }

  employeeService.getEmployee(JSON.parse(localStorage.getItem(EMPLOYEE_KEY)).id).subscribe((data: any) => {
    localStorage.setItem(EMPLOYEE_KEY, JSON.stringify(data));
  });
  navbarService.updateNavbar();

  return true
}
