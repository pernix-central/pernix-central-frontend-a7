import { Notification } from '@models/notification';

export interface NotificationData {
    notificationData: Notification;
    isNew: boolean;
}
