import { Employee } from '@models/employee';

export interface ApprovedDayData {
    employeeData: Employee
}