import { RequestType } from '@enums/request-type';
import { WalletRequest } from '@models/wallet-request';

export interface WalletRequestData {
    walletRequestData: WalletRequest;
    request_type: RequestType;
}