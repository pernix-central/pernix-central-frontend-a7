export interface CelebrationData {
    title: string;
    text: string;
    celebrationData: string;
}