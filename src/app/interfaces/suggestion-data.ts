import { Suggestion } from '@models/suggestion';

export interface SuggestionData {
    suggestionData: Suggestion;
}
