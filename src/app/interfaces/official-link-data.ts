import { OfficialLink } from '@models/official-link';

export interface OfficialLinkData {
    officialLinkData: OfficialLink;
    isNew: boolean;
}