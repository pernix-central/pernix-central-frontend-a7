import { NotificationMessage } from '@models/notification-message';

export interface NotificationMessageData {
    notificationMessageData: NotificationMessage[];
}
