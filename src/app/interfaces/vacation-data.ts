import { VacationRequest } from '@models/vacation-request';

export interface VacationRequestData {
    vacationRequestData: VacationRequest;
    requestedDaysList: string[];
}