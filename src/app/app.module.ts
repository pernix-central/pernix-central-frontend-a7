import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';

import { RoutesModule } from './routes/routes.module';
import { ComponentsModule } from '@components/components.module';

import { LoginModule } from './login/login.module';
import { EmployeesModule } from './employees/employees.module';
import { SuggestionsModule } from './suggestions/suggestions.module';
import { VacationsModule } from './vacations/vacations.module';
import { ChecklistModule } from './checklist/checklist.module';
import { OfficialLinksModule } from './official-links/official-links.module';
import { NotificationsMessagesModule } from './notification-messages/notification-messages.module';
import { WalletModule } from './wallet/wallet.module';

import { StorageService, LocalStorage } from '@services/storage.service';
import { LoginService } from './login/login.service';

import { AddCommentsDialogComponent } from '@dialogs/add-comments-dialog/add-comments-dialog.component';
import { CelebrationDialogComponent } from '@dialogs/celebration-dialog/celebration-dialog.component';
import { ChangePasswordDialogComponent } from '@dialogs/change-password-dialog/change-password-dialog.component';

import { VacationDetailsComponent } from './vacations/vacation-details/vacation-details.component';

import { PasswordRecoveryComponent } from './password-recovery/password-recovery.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { RecoveryService } from './services/recovery.service';

import { LoginState } from './login/login.state';
import { NgxsModule } from '@ngxs/store';
import { NgxsReduxDevtoolsPluginModule } from '@ngxs/devtools-plugin';

@NgModule({
  declarations: [
    AppComponent,
    AddCommentsDialogComponent,
    CelebrationDialogComponent,
    VacationDetailsComponent,
    ChangePasswordDialogComponent,
    PasswordRecoveryComponent,
    ResetPasswordComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    RoutesModule,
    HttpClientModule,
    ComponentsModule,
    LoginModule,
    EmployeesModule,
    SuggestionsModule,
    VacationsModule,
    ChecklistModule,
    OfficialLinksModule,
    NotificationsMessagesModule,
    WalletModule,
    NgxsModule.forRoot([LoginState]),
    NgxsReduxDevtoolsPluginModule.forRoot(),
  ],
  providers: [
    { provide: StorageService, useClass: LocalStorage },
    LoginService,
    RecoveryService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
