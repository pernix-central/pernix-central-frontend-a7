import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { CelebrationData } from '@interfaces/celebration-data';

@Component({
  selector: 'app-celebration-dialog',
  templateUrl: './celebration-dialog.component.html'
})

export class CelebrationDialogComponent {
  celebrationList: string[];
  title: string;
  text: string;

  constructor(
    public dialogRef: MatDialogRef<CelebrationDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: CelebrationData,
  ) {
    this.title = data.title;
    this.text = data.text;
    if (!!data.celebrationData) {
      this.celebrationList = data.celebrationData.replace(/['"]+/g, '').slice(1, -1).split(",");
    }
  }

  cancel(): void {
    this.dialogRef.close()
  }
}
