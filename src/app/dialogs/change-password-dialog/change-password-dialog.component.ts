import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { Employee } from "@models/employee";

import { EmployeesService } from "../../employees/employees.service";
import { NotificationsService } from "@services/notifications.service";

import { environment } from "@environments/environment";

const EMPLOYEE_KEY = environment.employee_key;

@Component({
  selector: 'app-change-password-dialog',
  templateUrl: './change-password-dialog.component.html'
})

export class ChangePasswordDialogComponent {
  password: string;
  password_confirmation: string;
  isLoading: boolean;
  currentEmployee: Employee;

  constructor(
    public dialogRef: MatDialogRef<ChangePasswordDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: Employee,
    private notificationsService: NotificationsService,
    private employeesService: EmployeesService,
  ) {
    this.isLoading = false;
    this.currentEmployee = data;
  }

  savePassword(): void {
    if (this.password == this.password_confirmation && !!(this.password)) {
      this.isLoading = true;
      this.employeesService.updatePassword(this.currentEmployee, this.password, this.password_confirmation).subscribe((data: any) => {
        if (data.id == this.currentEmployee.id) {
          this.notificationsService.showNotification('Password has been changed successfully', 'success');
          this.dialogRef.close();
        }
      })
    } else {
      this.notificationsService.showNotification('Passwords do not match', 'danger');
    }
  }

  close(): void {
    this.dialogRef.close();
  }
}