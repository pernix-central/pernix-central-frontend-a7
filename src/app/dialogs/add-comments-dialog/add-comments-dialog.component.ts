import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { NotificationsService } from '@services/notifications.service';

import { Comment } from '@models/comment';
import { Suggestion } from '@models/suggestion';
import { SuggestionData } from '@interfaces/suggestion-data';

@Component({
  selector: 'app-add-comments-dialog',
  templateUrl: './add-comments-dialog.component.html'
})
export class AddCommentsDialogComponent {
  suggestion: Suggestion;
  comment: Comment;

  constructor(
    private notificationsService: NotificationsService,
    public dialogRef: MatDialogRef<AddCommentsDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SuggestionData,
  ) {
    this.suggestion = data.suggestionData;
    this.comment = new Comment();
    this.comment.suggestion_id = this.suggestion.id;
    this.comment.anonymus = true;
  }

  confirm() {
    if (!!this.comment.text) {
      this.dialogRef.close({ comment: this.comment, create: true });
    } else {
      this.notificationsService.showNotification('Please fill out the required fields', 'danger');
    }
  }

  cancel() {
    this.dialogRef.close({ create: false })
  }
}
