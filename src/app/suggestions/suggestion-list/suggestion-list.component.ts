import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';

import { SuggestionsService } from '../suggestions.service';
import { NotificationsService } from '@services/notifications.service';
import { StorageService } from '@services/storage.service';

import { Suggestion } from '@models/suggestion';
import { Employee } from '@models/employee';
import { environment } from '@environments/environment';

import { SuggestionFormComponent } from '../suggestion-form/suggestion-form.component';

const EMPLOYEE_KEY = environment.employee_key;

@Component({
  selector: 'app-suggestion-list',
  templateUrl: './suggestion-list.component.html'
})
export class SuggestionListComponent implements OnInit {
  suggestions: Suggestion[];
  employee: Employee;
  private storage: Storage;

  constructor(
    private suggestionsService: SuggestionsService,
    private notificationsService: NotificationsService,
    private storageService: StorageService,
    private dialog: MatDialog
  ) { 
    this.storage = this.storageService.get();
  }

  addSuggestion() {
    const dialogRef = this.dialog.open(SuggestionFormComponent, {
      width: '85%',
      data: {
        suggestionData: new Suggestion(),
        currentEmployee: this.employee
      },
      disableClose: true
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getSuggestions();
      }
    })
  }

  getSuggestions() {
    this.suggestions = null;
    this.suggestionsService.getSuggestions().subscribe((data: any) => {
      this.suggestions = data;
    }, (_error: any) => {
      this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
    })
  }

  ngOnInit() {
    this.getSuggestions();
    this.employee = JSON.parse(this.storage.getItem(EMPLOYEE_KEY));
  }
}
