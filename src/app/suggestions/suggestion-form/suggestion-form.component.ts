import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { SuggestionsService } from '../suggestions.service';
import { NotificationsService } from '@services/notifications.service';

import { Suggestion } from '@models/suggestion';
import { Employee } from '@models/employee';
import { environment } from '@environments/environment';
import { SuggestionData } from '@interfaces/suggestion-data';

import Quill from 'quill'
import BlotFormatter from 'quill-blot-formatter/dist';
Quill.register('modules/blotFormatter', BlotFormatter);

const EMPLOYEE_KEY = environment.employee_key;

@Component({
  selector: 'app-suggestion-form',
  templateUrl: './suggestion-form.component.html'
})
export class SuggestionFormComponent {
  suggestion: Suggestion;
  isLoading: boolean;
  currentEmployee: Employee = JSON.parse(localStorage.getItem(EMPLOYEE_KEY));
  modules = {};

  constructor(
    private suggestionsService: SuggestionsService,
    private notificationsService: NotificationsService,
    public dialogRef: MatDialogRef<SuggestionFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: SuggestionData
  ) {
    this.suggestion = data.suggestionData;

    this.modules = {
      blotFormatter: {
        // empty object for default behaviour.
      },
      toolbar: {
        container: [
          ['bold', 'italic', 'underline', 'strike'], // toggled buttons
          ['blockquote', 'code-block'],
          [{ 'size': ['small', false, 'large', 'huge'] }], // custom dropdown
          [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
          [{ 'font': [] }],
          [{ 'list': 'ordered' }, { 'list': 'bullet' }],
          [{ 'script': 'sub' }, { 'script': 'super' }], // superscript/subscript
          [{ 'indent': '-1' }, { 'indent': '+1' }], // outdent/indent
          [{ 'color': [] }, { 'background': [] }], // dropdown with defaults from theme
          [{ 'align': [] }],
          ['clean'], // remove formatting button
          ['link', 'image'] // link and image, video
        ]
      }
    }
  }

  toggle() {
    this.isLoading = !this.isLoading;
  }

  addSuggestion(suggestion: Suggestion) {
    this.toggle();
    this.suggestionsService.createSuggestion(suggestion, this.currentEmployee.id).subscribe((data: any) => {
      if (data.title == suggestion.title) {
        this.dialogRef.close(true);
        this.notificationsService.showNotification('Suggestion created successfully', 'success');
      } else {
        this.notificationsService.showNotification('Please fill out the required fields', 'danger');
        this.toggle();
      }
    }, (_error: any) => {
      this.dialogRef.close(false);
      this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
    })
  }

  cancel() {
    this.dialogRef.close(false);
  }
}
