import { Injectable } from '@angular/core';
import { Router, ActivatedRouteSnapshot } from '@angular/router';

import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { SuggestionsService } from './suggestions.service';

@Injectable({
  providedIn: 'root'
})
export class SuggestionResolverService {
  private suggestionId: number;

  constructor(
    private router: Router,
    private suggestionsService: SuggestionsService
  ) { }

  resolve(route: ActivatedRouteSnapshot):

    Observable<any> {
    this.suggestionId = +route.paramMap.get('suggestion_id');

    return this.suggestionsService.getSuggestion(this.suggestionId).pipe(
      take(1),
      map(suggestion => {
        let response: any;
        if (suggestion) {
          response = suggestion;
        } else {
          this.router.navigate(['suggestions', 'list']);
          response = null;
        }
        return response;
      })
    )
  }
}
