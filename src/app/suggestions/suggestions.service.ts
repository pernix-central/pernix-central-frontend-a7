import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '@services/storage.service';

import { Suggestion } from '@models/suggestion';
import { environment } from '@environments/environment';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class SuggestionsService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  getSuggestions() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    };

    const url = `${this.apiUrl}/v1/suggestions`;

    return this.http.get(url, this.httpOptions);
  }

  getSuggestion(suggestionId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    };

    const url = `${this.apiUrl}/v1/suggestions/${suggestionId}`;

    return this.http.get(url, this.httpOptions);
  }

  createSuggestion(suggestion: Suggestion, employeeId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    };

    const url = `${this.apiUrl}/v1/suggestions`;

    const payload = {
      suggestion: {
        title: suggestion.title,
        description: suggestion.description,
        employee_id: employeeId
      }
    };

    return this.http.post(url, payload, this.httpOptions);
  }

  markSuggestionResolved(suggestion: Suggestion) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    };

    const url = `${this.apiUrl}/v1/suggestions/${suggestion.id}/mark-resolved`;

    const payload = {
      suggestion: {}
    };

    return this.http.put(url, payload, this.httpOptions);
  }

  markSuggestionUnresolved(suggestion: Suggestion) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    };

    const url = `${this.apiUrl}/v1/suggestions/${suggestion.id}/mark-unresolved`;

    const payload = {
      suggestion: {}
    };

    return this.http.put(url, payload, this.httpOptions);
  }
}
