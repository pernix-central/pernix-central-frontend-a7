import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '@services/storage.service';

import { Checklist } from '@models/checklist';
import { environment } from '@environments/environment';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class ChecklistService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  getTasks() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    };

    const url = `${this.apiUrl}/v1/checklist/user_tasks`;

    return this.http.get(url, this.httpOptions)
  }

  createTask(checklistTask: Checklist, employeeId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    };

    const url = `${this.apiUrl}/v1/checklist`;

    const payload = {
      checklist: {
        task_name: checklistTask.task_name,
        task_type: checklistTask.task_type,
        employee_id: employeeId
      }
    };

    return this.http.post(url, payload, this.httpOptions);
  }

  updateTask(checklistTask: Checklist, employeeId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    };

    const url = `${this.apiUrl}/v1/checklist/${checklistTask.id}`;

    const payload = {
      checklist: {
        task_name: checklistTask.task_name,
        task_type: checklistTask.task_type,
        employee_id: employeeId,
      }
    };

    return this.http.put(url, payload, this.httpOptions);
  }

  destroyTask(checklistTask: Checklist) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    };

    const url = `${this.apiUrl}/v1/checklist/${checklistTask.id}`;

    return this.http.delete(url, this.httpOptions);
  }

  markTaskCompleted(checklistTask: Checklist) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    };

    const payload = {
      checklist: {
        id: checklistTask.id,
      }
    };

    const url = `${this.apiUrl}/v1/checklist_completion`;

    return this.http.post(url, payload, this.httpOptions);
  }

  markTaskUncompleted(checklistTask: Checklist) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    };

    const url = `${this.apiUrl}/v1/checklist_completion/${checklistTask.id}`;

    return this.http.delete(url, this.httpOptions);
  }

}
