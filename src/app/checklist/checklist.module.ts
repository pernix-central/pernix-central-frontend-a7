import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from '@components/components.module';

import { ChecklistComponent } from './checklist.component';
import { ChecklistListComponent } from './checklist-list/checklist-list.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [
    ChecklistComponent,
    ChecklistListComponent
  ],
  providers: [],
  bootstrap: [ChecklistComponent]
})
export class ChecklistModule { }
