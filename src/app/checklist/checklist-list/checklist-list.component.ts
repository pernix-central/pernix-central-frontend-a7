import { Component, OnInit } from '@angular/core';

import { ChecklistService } from '../checklist.service';
import { NotificationsService } from '@services/notifications.service';
import { PermissionsService } from '@services/permissions.service';

import { Checklist } from '@models/checklist';
import { Employee } from '@models/employee';
import { TaskType } from '@enums/checklist-task-type';
import { Status } from '@enums/status';
import { environment } from '@environments/environment';

const EMPLOYEE_KEY = environment.employee_key;
@Component({
  selector: 'app-checklist',
  templateUrl: './checklist-list.component.html'
})
export class ChecklistListComponent implements OnInit {
  currentUser: Employee = JSON.parse(localStorage.getItem(EMPLOYEE_KEY));
  currentEditingTask: Checklist;
  checklist: Checklist[];
  checklistCompletions: Checklist[];
  checklistTask: Checklist;
  isGlobal: boolean;
  addingNewTask: boolean;

  constructor(
    private permissionsService: PermissionsService,
    private checklistService: ChecklistService,
    private notificationsService: NotificationsService
  ) { }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin();
  }

  getUserName() {
    return this.currentUser.first_name + " " + this.currentUser.last_name;
  }

  getTodaysDate() {
    const today = new Date(Date.now());
    return today.toDateString();
  }

  toggleGlobal(checklistTask: Checklist) {
    checklistTask.is_global = !checklistTask.is_global;
    checklistTask.task_type = checklistTask.is_global ? TaskType.GLOBAL : TaskType.CUSTOM;
  }

  fillChecklist() {
    this.checklistService.getTasks().subscribe((data: any) => {
      this.checklist = [];
      this.checklistCompletions = [];
      this.checklist = data.tasks.sort((a, b) => {
        return (a.task_type === b.task_type) ? 0 : a.task_type === "custom" ? 1 : -1
      });
      data.completions.forEach((t) => {
        this.checklist.forEach((c) => {
          if (t.task_id == c.id) {
            c.is_completed = true;
            this.checklistCompletions.push(c);
            this.findAndRemoveTask(this.checklist, c)
          }
        });
      });
    });
  }

  addTask(checklistTask: Checklist) {
    checklistTask.task_type = checklistTask.is_global ? TaskType.GLOBAL : TaskType.CUSTOM;
    this.checklistService.createTask(this.checklistTask, this.currentUser.id).subscribe((data: any) => {
      if (data.task_name == checklistTask.task_name) {
        this.getAllTasks();
        this.notificationsService.showNotification('Checklist task created successfully', 'success');
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    });
    checklistTask.is_new = false;
    this.addingNewTask = false;
  }

  editTask(checklistTask: Checklist) {
    checklistTask.task_type = checklistTask.is_global ? TaskType.GLOBAL : TaskType.CUSTOM;
    this.checklistService.updateTask(checklistTask, this.currentUser.id).subscribe((data: any) => {
      if (data.task_name == checklistTask.task_name) {
        this.getAllTasks();
        this.notificationsService.showNotification('Checklist task updated successfully', 'success');
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    });
  }

  deleteTask(checklistTask: Checklist) {
    this.checklistService.destroyTask(checklistTask).subscribe((data: any) => {
      if (data == Status.OK) {
        this.getAllTasks();
        this.notificationsService.showNotification('Checklist task deleted successfully', 'success');
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    });
  }

  addTaskInput() {
    this.checklistTask.is_editable = true;
    this.addingNewTask = true;
    this.checklist.push(this.checklistTask);
  }

  closeEditOnClick(evt: PointerEvent) {
    const target = evt.target as HTMLBodyElement;
    if (target.className == 'card-body' && !!(this.currentEditingTask)) {
      this.currentEditingTask.is_editable = false;
    }
  }

  activateEditInput(checklistTask: Checklist) {
    if (this.currentEditingTask == undefined) {
      this.currentEditingTask = checklistTask;
    }
    if (this.currentEditingTask != checklistTask) {
      this.currentEditingTask.is_editable = false;
      this.currentEditingTask = checklistTask;
    }
    if (!this.canAccess() && checklistTask.task_type == TaskType.GLOBAL) {
      this.notificationsService.showNotification('Only an administrator can edit global tasks', 'warning');
      return;
    }
    checklistTask.is_editable = true;
    if (checklistTask.task_type == TaskType.CUSTOM) {
      checklistTask.is_global = false;
    }
    if (checklistTask.task_type == TaskType.GLOBAL) {
      checklistTask.is_global = true;
    }
  }

  removeTaskInput(checklistTask: Checklist, index: number) {
    checklistTask.is_editable = false;
    this.addingNewTask = false;
    if (checklistTask.is_new) {
      this.checklist.splice(index, 1);
    }
  }

  markTask(event:any, task:Checklist) {
    const checkboxIsChecked = event.target.checked;

    if (checkboxIsChecked) {
      this.checklistService.markTaskCompleted(task).subscribe();
      this.findAndRemoveTask(this.checklist, task)
      this.checklistCompletions.push(task)
    } else {
      this.checklistService.markTaskUncompleted(task).subscribe();
      this.findAndRemoveTask(this.checklistCompletions, task); 
      this.checklist.push(task);
    }
  }

  findAndRemoveTask(checklist: Checklist[], checklistTask: Checklist) {
    for (let i = 0; i < checklist.length; i++) {
      if (checklist[i].id == checklistTask.id) {
        checklist.splice(i, 1);
      }
    }
  }

  getAllTasks() {
    this.addingNewTask = false;
    this.checklistTask = new Checklist();
    this.fillChecklist();
  }

  ngOnInit(): void {
    this.getAllTasks();
  }
}
