import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '@services/storage.service';

import { environment } from '@environments/environment';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class EmployeesService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  createEmployee(employee: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/employees`;

    let payload = {
      employee: {
        first_name: employee.first_name,
        last_name: employee.last_name,
        birthdate: employee.birthdate,
        email: employee.email,
        has_car: employee.has_car,
        on_vacation: employee.on_vacation,
        can_be_mentor: employee.can_be_mentor,
        blog_url: employee.blog_url,
        computer_serial_number: employee.computer_serial_number,
        mac_address: employee.mac_address,
        hire_date: employee.hire_date,
        active: employee.active,
        vacation_control: employee.vacation_control,
        phone_number: employee.phone_number,
        id_number: employee.id_number,
        role_id: employee.role.id
      }
    };

    return this.http.post(url, payload, this.httpOptions);
  }

  getEmployees() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/employees`;

    return this.http.get(url, this.httpOptions)
  }

  getEmployee(employeeId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/employees/${employeeId}`;

    return this.http.get(url, this.httpOptions)
  }

  updateEmployee(employee: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/employees/${employee.id}`;

    let payload = {
      employee: {
        first_name: employee.first_name,
        last_name: employee.last_name,
        birthdate: employee.birthdate,
        email: employee.email,
        has_car: employee.has_car,
        on_vacation: employee.on_vacation,
        can_be_mentor: employee.can_be_mentor,
        blog_url: employee.blog_url,
        computer_serial_number: employee.computer_serial_number,
        mac_address: employee.mac_address,
        hire_date: employee.hire_date,
        active: employee.active,
        vacation_control: employee.vacation_control,
        phone_number: employee.phone_number,
        id_number: employee.id_number,
        role_id: employee.role.id
      }
    };


    return this.http.put(url, payload, this.httpOptions);
  }

  updatePassword(employee: any, password: string, password_confirmation: string) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/employees/${employee.id}/change_password`;

    let payload = {
      employee: {
        password: password,
        password_confirmation: password_confirmation
      }
    }
    return this.http.patch(url, payload, this.httpOptions);

  }

  updateEmployeeProfileImages(employee: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/employees/${employee.id}/update_image`;

    var fd = new FormData();

    if (employee.attachment) {
      fd.append(`employee[profile]`, employee.attachment, employee.attachment.name);
    } else {
      fd.append(`employee[profile]`, 'remove');
    }

    return this.http.patch(url, fd, this.httpOptions);
  }

  activeEmployee(employeeId: number, active: boolean) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/employees/${employeeId}/change_status`;

    let payload = {
      employee: {
        active: active
      }
    };

    return this.http.put(url, payload, this.httpOptions);
  }
}
