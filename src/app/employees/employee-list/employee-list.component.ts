import { Component, OnInit } from '@angular/core';
import { Sort } from '@angular/material/sort';

import { EmployeesService } from '../employees.service';
import { PermissionsService } from '@services/permissions.service';

import { Employee } from '@models/employee';
import { environment } from '@environments/environment';

const EMPLOYEE_KEY = environment.employee_key;

@Component({
  selector: 'app-employee-list',
  templateUrl: './employee-list.component.html'
})
export class EmployeeListComponent implements OnInit {
  currentEmployee: Employee = JSON.parse(localStorage.getItem(EMPLOYEE_KEY));
  employees: Employee[];
  sortedData: Employee[];
  inactiveEmployees: Employee[];
  sortedInactiveEmployees: Employee[];
  scheduleOnly: boolean;

  constructor(
    private employeesService: EmployeesService,
    private permissionsService: PermissionsService
  ) { }

  daySchedule(employee: Employee) {
    let schedule = '';
    const todayDate = new Date();
    const todayDay = todayDate.getDay();

    if (todayDay - 1 < 5) {
      const workDay = employee.schedule.work_days[todayDay - 1];

      workDay.periods.forEach((p, i) => {
        let period = `${p.start_time} to ${p.end_time}, `;
        if (i == workDay.periods.length - 1) {
          period = period.replace(/,\s*$/, '');
        }
        schedule += period;
      });
    } else {
      schedule = 'Week end';
    }

    return schedule;
  }

  dayScheduleWFH(employee: Employee) {
    let wfh = false;
    const todayDate = new Date();
    const todayDay = todayDate.getDay();

    if (todayDay - 1 < 5) {
      const workDay = employee.schedule.work_days[todayDay - 1];

      wfh = workDay.wfh;
    } else {
      wfh = true;
    }

    return wfh;
  }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin();
  }

  sortActiveEmployees(sort: Sort) {
    this.sortedData = this.sortData(sort, this.sortedData)
  }

  sortInactiveEmployees(sort: Sort) {
    this.sortedInactiveEmployees = this.sortData(sort, this.sortedInactiveEmployees)
  }

  private sortData(sort: Sort, employeeList: Employee[]) {
    const data = employeeList.slice();
    if (!sort.active || sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name': return this.compare(a.first_name, b.first_name, isAsc);
        case 'last_name': return this.compare(a.last_name, b.last_name, isAsc);
        default: return 0;
      }
    });
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  filterListByNameAndLastNameAndEmail(searchTarget: string, employeesList: Employee[]): Employee[] {
    searchTarget = searchTarget.toLowerCase();
    return employeesList.filter((employee: Employee) => {
      return employee.first_name.toLowerCase().includes(searchTarget) ||
        employee.last_name.toLowerCase().includes(searchTarget) ||
        employee.email.toLowerCase().includes(searchTarget);
    });
  }

  filterActiveEmployees(searchTarget: string, employeesList: Employee[]): void {
    this.sortedData = (searchTarget.length >= 3) ?
      this.filterListByNameAndLastNameAndEmail(searchTarget, employeesList) :
      employeesList;
  }

  filterInactiveEmployees(searchTarget: string, employeesList: Employee[]): void {
    this.sortedInactiveEmployees = (searchTarget.length >= 3) ?
      this.filterListByNameAndLastNameAndEmail(searchTarget, employeesList) :
      employeesList;
  }

  path(employee: Employee): (string | number)[] {
    return employee.id == this.currentEmployee.id ?
      ['../../', 'my-profile', employee.id, 'details'] : ['../../', 'employees', employee.id, 'details'];
  }

  private filterByActive(isActive: boolean, employees: Employee[]) {
    return employees.filter((employee) => employee.active == isActive);
  }

  private activeEmployees(data: any) {
    this.employees = this.filterByActive(true, data);
    this.sortedData = this.employees.slice();
  }

  private getEmployees() {
    if (this.permissionsService.currentEmployeeIsAdmin()) {
      this.employeesService.getEmployees().subscribe((data: any) => {
        this.activeEmployees(data);
        this.inactiveEmployees = this.filterByActive(false, data);
        this.sortedInactiveEmployees = this.inactiveEmployees.slice();
      }, console.error);
    } else {
      this.employeesService.getEmployees().subscribe((data: any) => {
        this.employees = data;
        this.sortedData = this.employees.slice();
      }, console.error);
    }
  }

  ngOnInit() {
    this.scheduleOnly = false;
    this.getEmployees();
  }
}
