import { Injectable, inject } from '@angular/core';
import { Router, ActivatedRouteSnapshot, ResolveFn } from '@angular/router';

import { Observable } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { EmployeesService } from './employees.service';
import { StorageService } from '@services/storage.service';

import { environment } from '@environments/environment';

const EMPLOYEE_KEY = environment.employee_key;

export const EmployeeResolver: ResolveFn<any> = (
  route: ActivatedRouteSnapshot,
): Observable<any> => {
  const storageService:StorageService = inject(StorageService);
  const employeesService:EmployeesService = inject(EmployeesService);
  const router: Router = inject(Router);
  
  const storage:Storage = storageService.get();
  const employeeId:number = +route.paramMap.get('employee_id');
  const savedEmployee:any = storage.getItem(EMPLOYEE_KEY)

  return employeesService.getEmployee(employeeId).pipe(
    take(1),
    map(employee => {
      let response: any;
      if (savedEmployee.id == employeeId) {
        response = savedEmployee;
      } else if (employee) {
        response = employee;
      } else {
        router.navigate(['employees', 'list']);
        response = null;
      }
      return response;
    })
  );
}