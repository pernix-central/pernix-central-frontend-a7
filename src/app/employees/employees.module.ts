import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatTabsModule } from '@angular/material/tabs';
import { ReactiveFormsModule } from '@angular/forms';

import { ComponentsModule } from '@components/components.module';

import { EmployeesComponent } from './employees.component';
import { EmployeeListComponent } from './employee-list/employee-list.component';
import { EmployeeDetailsComponent } from './employee-details/employee-details.component';
import { EmployeeFormComponent } from './employee-form/employee-form.component';

import { NgxMaskDirective, NgxMaskPipe, provideNgxMask } from 'ngx-mask' 

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    MatTabsModule,
    ReactiveFormsModule,
    NgxMaskDirective,
    NgxMaskPipe
  ],
  declarations: [
    EmployeesComponent,
    EmployeeListComponent,
    EmployeeDetailsComponent,
    EmployeeFormComponent,
  ],
  providers: [provideNgxMask()],
  bootstrap: [EmployeesComponent]
})
export class EmployeesModule { }
