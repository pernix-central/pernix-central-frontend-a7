import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { EmployeesService } from '../employees.service';
import { NotificationsService } from '@services/notifications.service';
import { PermissionsService } from '@services/permissions.service';
import { SweetAlertService } from '@services/sweet-alert.service';

import { Employee } from '@models/employee';
import { VacationRequest } from '@models/vacation-request';
import { environment } from '@environments/environment';

const EMPLOYEE_KEY = environment.employee_key;

@Component({
  selector: 'app-employee-details',
  templateUrl: './employee-details.component.html'
})
export class EmployeeDetailsComponent implements OnInit {
  currentEmployee: Employee = JSON.parse(localStorage.getItem(EMPLOYEE_KEY));
  private apiUrl: string;
  employee: Employee;
  residentIdMask: any;
  nationalIdMask: any;
  phoneMask: any;
  vacationRequestList: VacationRequest[];
  isVacationListEmpty: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private permissionsService: PermissionsService,
    private employeesService: EmployeesService,
    private notificationsService: NotificationsService,
    private sweetAlertService: SweetAlertService,
  ) {
    this.apiUrl = environment.apiUrl;
    this.nationalIdMask = [/\d/, ' ', '-', ' ', /\d/, /\d/, /\d/, /\d/, ' ', '-', ' ', /\d/, /\d/, /\d/, /\d/];
    this.residentIdMask = [/\d/, /\d/, ' ', '-', ' ', /\d/, /\d/, /\d/, /\d/, /\d/, ' ', '-', ' ', /\d/, /\d/, /\d/, /\d/, /\d/];
    this.phoneMask = [/\d/, /\d/, /\d/, /\d/, ' ', '-', ' ', /\d/, /\d/, /\d/, /\d/];
  }

  mask(): any {
    return {
      mask: (value: any) => {
        if (value.length <= 15)
          return this.nationalIdMask;
        else
          return this.residentIdMask;
      },
      guide: false
    };
  }

  ngOnInit() {
    this.route.data.subscribe((data: { employee: any }) => {
      this.employee = data.employee;
    });
  }

  counter(cant_items: number) {
    return new Array(cant_items);
  }

  isApprentice() {
    return this.permissionsService.employeeIsApprentice(this.employee)
  }

  isCrafter() {
    return this.permissionsService.currentEmployeeIsCrafter();
  }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin() || this.permissionsService.isCurrentEmployee(this.employee);
  }

  canAccessVacations() {
    return (this.permissionsService.currentEmployeeIsCrafter() || this.permissionsService.currentEmployeeIsAdmin()) && this.permissionsService.isCurrentEmployee(this.employee);
  }

  haveHistoryVacationButton() {
    return !this.permissionsService.isCurrentEmployee(this.employee) && !this.permissionsService.employeeIsAdmin(this.employee);
  }

  goToVacationsHistory() {
    if (this.permissionsService.currentEmployeeIsAdmin()) {
      this.router.navigate(['../../../', 'vacations', this.employee.id, 'vacation_history']);
    } else {
      this.router.navigate(['../../../', 'vacations', 'history']);
    }
  }

  workHoursFormat(): string {
    var total_work_hours = this.employee.schedule.total_work_hours;
    var hours = Math.floor(total_work_hours);
    var minutes = (total_work_hours - hours) * 60;
    if (minutes != 0) {
      return hours + 'h ' + minutes + 'min';
    }
    return hours + 'h';
  }

  activeEmployee() {
    this.sweetAlertService.showConfirmationDialog().fire({
      html: `<b>Are you sure you want to activate the user ${this.employee.first_name}?</b>`,
    }).then((result) => {
      if (result.isConfirmed) {
        this.employeesService.activeEmployee(this.employee.id, true).subscribe((_data: any) => {
          this.employee.active = true;
          this.notificationsService.showNotification('The user ' + this.employee.first_name + ' has been activated successfully', 'success');
        }, (_error: any) => {
          this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
        })
      } else if (
        result.dismiss === this.sweetAlertService.showConfirmationDialog().DismissReason.cancel
      ) {
      }
    })
  }

  haveActivateButton() {
    return !this.employee.active;
  }

  updateEmployeeKey() {
    if (this.currentEmployee.id === this.employee.id) {
      this.currentEmployee.employee_available_days = this.employee.employee_available_days;
      localStorage.setItem(EMPLOYEE_KEY, JSON.stringify(this.currentEmployee));
    }
  }
}
