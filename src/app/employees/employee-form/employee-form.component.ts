import { AfterViewInit, Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Validators, UntypedFormGroup, UntypedFormBuilder } from '@angular/forms';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { MatStepper } from '@angular/material/stepper';
import { MatDialog } from '@angular/material/dialog';

import { EmployeesService } from '../employees.service';
import { ScheduleService } from '@services/schedule.service';
import { NotificationsService } from '@services/notifications.service';
import { PermissionsService } from '@services/permissions.service';
import { RolesService } from '@services/roles.service';
import { StorageService } from '@services/storage.service';
import { SweetAlertService } from '@services/sweet-alert.service';
import { NavbarService } from '@services/navbar.service';

import { Employee } from '@models/employee';
import { Period } from '@models/period';
import { Role } from '@models/role';
import { Roles } from '@enums/user-roles';
import { Status } from '@enums/status';
import { environment } from '@environments/environment';

import { ChangePasswordDialogComponent } from '@dialogs/change-password-dialog/change-password-dialog.component';

import * as moment from 'moment';

const EMPLOYEE_KEY = environment.employee_key;
@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false }
  }]
})
export class EmployeeFormComponent implements OnInit, AfterViewInit {
  currentEmployee: Employee = JSON.parse(localStorage.getItem(EMPLOYEE_KEY));
  employee: Employee;
  validRoles: Role[];
  validSkillLevels: any;
  isNew: boolean;
  residentIdMask: any;
  nationalIdMask: any;
  nationalIdNumber: any | null;
  residentIdNumber: any | null;
  currentIdType: any | null;
  phoneMask: any;
  emailRegex: RegExp;
  emailForm: UntypedFormGroup;
  isLoading: boolean;
  hoursArray: string[];
  previousEmployee: Employee;
  private apiUrl: string;
  private storage: Storage;
  private employeeRole: Role;
  private employeeHireDate: string;
  @ViewChild('stepper') stepper: MatStepper;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private employeesService: EmployeesService,
    private scheduleService: ScheduleService,
    private permissionsService: PermissionsService,
    private rolesService: RolesService,
    private notificationsService: NotificationsService,
    private storageService: StorageService,
    private navbarService: NavbarService,
    private fb: UntypedFormBuilder,
    private dialog: MatDialog,
    private sweetAlertService: SweetAlertService,
  ) {
    this.apiUrl = environment.apiUrl;
    this.validSkillLevels = [
      { name: 'Novice', level: '1' },
      { name: 'Beginner', level: '2' },
      { name: 'Competent', level: '3' },
      { name: 'Proficient', level: '4' },
      { name: 'Expert', level: '5' }
    ];

    this.nationalIdMask = "0 - 0000 - 0000"
    this.residentIdMask = "00 - 00000 - 00000"
    this.phoneMask = "0000 - 0000"
    this.storage = this.storageService.get();

    this.hoursArray = [];
    this.fillHoursArray();
  }

  isNationalId() {
    return this.currentIdType == 'National'
  }

  isResidentId() {
    return this.currentIdType == 'Resident'
  }

  updateIdNumberField(event: any) {
    this.currentIdType = event.value;
  }

  setCorrespodingIdNumberValue(type: string) {
    if (type == 'National') {
      this.nationalIdNumber = this.employee.id_number;
    } else {
      this.residentIdNumber = this.employee.id_number;
    }
  }

  moveStepper(index: number) {
    this.stepper.selectedIndex = index;
  }

  isApprentice(employee: Employee) {
    return this.permissionsService.employeeIsApprentice(employee);
  }

  currentEmployeeIsAdmin() {
    return this.permissionsService.currentEmployeeIsAdmin()
  }

  toggle() {
    this.isLoading = !this.isLoading;
  }

  compareRoles(r1: any, r2: any): boolean {
    if (r1 != undefined && r2 != undefined) {
      return r1.tier === r2.tier;
    }
  }

  validateEmail(email) {
    if (email.pristine) {
      return null;
    }
    const emailRegex = /^[\w-\.]+@(pernixlabs|pernix\-solutions)\.com$/;
    email.markAsTouched();
    if (emailRegex.test(email.value)) {
      return null;
    }
    return {
      invalidEmail: true
    };
  }

  filteredPeriods(dayIndex: number) {
    return this.employee.schedule.work_days[dayIndex].periods.filter((p: any) => p._destroy === false);
  }

  fillHoursArray() {
    for (let index = 1; index <= 24; index++) {
      if (index < 10) {
        this.hoursArray.push(`0${index}:00`);
      } else {
        this.hoursArray.push(`${index}:00`);
      }
    }
  }

  addEmployee(employee: Employee) {
    if (employee.role) {
      this.toggle();
      employee.email = employee.email.toLowerCase();
      employee.id_number = this.getIdNumber();
      this.employeesService.createEmployee(employee).subscribe((data: any) => {
        if (data.message == 'Account created successfully') {
          this.notificationsService.showNotification('Employee created successfully', 'success');
          this.router.navigate(['employees', 'list']);
        } else {
          this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
          this.toggle();
        }
      }, (error: any) => {
        if (error.error.hasOwnProperty('id_number') || error.error.hasOwnProperty('email')) {
          if (error.error.id_number[0] == Status.ALREADY_TAKEN) {
            this.notificationsService.showNotification('A user with the same ID number already exists.', 'danger');
          } else if (error.error.email[0] == Status.ALREADY_TAKEN) {
            this.notificationsService.showNotification('A user with the same email already exists.', 'danger');
          }
        }
        this.toggle();
      });
    }
  }

  checkHireDate(employee: Employee) {
    if (employee.hire_date == this.employeeHireDate) {
      this.notificationsService.showNotification("Please update the employee's hire date.", 'danger');
      this.toggle();
    } else if (this.employeeHireDate > moment(employee.hire_date).format('YYYY-MM-DD')) {
      this.sweetAlertService.showConfirmationDialog().fire({
        html: `<b>Are you sure you want to update the hire date?</b><br><br>The new hire date 
        ${moment(employee.hire_date).format('MM-DD-YYYY')} is before the current hire date 
        ${moment(this.employeeHireDate).format('MM-DD-YYYY')}.`,
      }).then((result) => {
        if (result.isConfirmed) {
          this.updateEmployee(employee);
        } else (
          result.dismiss === this.sweetAlertService.showConfirmationDialog().DismissReason.cancel,
          this.toggle()
        )
      })
    } else {
      this.updateEmployee(employee);
    }
  }

  checkRoleUpdate(employee: Employee) {
    if (this.employeeRole != employee.role && this.employeeRole.tier == Roles.Apprentice) {
      this.toggle();
      this.sweetAlertService.showConfirmationDialog().fire({
        html: `<b>Are you sure you want to update the role to ${employee.first_name}?</b>
        <br><br>This action CANNOT be undone!`,
      }).then((result) => {
        if (result.isConfirmed) {
          this.checkHireDate(employee);
        } else (
          result.dismiss === this.sweetAlertService.showConfirmationDialog().DismissReason.cancel,
          this.toggle()
        )
      })
    } else {
      this.updateEmployee(employee);
    }
  }

  getIdNumber() {
    return this.isNationalId() ? this.nationalIdNumber : this.residentIdNumber;
  }

  updateEmployee(employee: Employee) {
    this.toggle();
    employee.id_number = this.getIdNumber();
    employee.email = employee.email.toLowerCase();
    this.employeesService.updateEmployee(employee).subscribe((data: any) => {
      if (data.id == this.employee.id) {
        this.employee = data;
        this.employee.id == this.currentEmployee.id ? this.notificationsService.showNotification('My profile updated successfully', 'success')
          : this.notificationsService.showNotification('Employee updated successfully', 'success');
        this.updateEmployeeKey();
        this.router.navigate(this.path());
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again. Verify no other users with the same ID number or email exist.', 'danger');
        this.toggle();
      }
    }, console.error);
  }

  addPeriod(day: any) {
    let newPeriod: Period = {
      start_time: '00:00', end_time: '00:00', _destroy: false
    }

    this.employee.schedule.work_days[day].periods.push(newPeriod);
  }

  removePeriod(day: any, period: any) {
    this.employee.schedule.work_days[day].periods[period]._destroy = true;
  }

  updateSchedule(employeeId: number, schedule: any) {
    const isAtLeastOneDayFilled = schedule.work_days.some(day => {
      return day.periods.some(period =>
        !period._destroy &&
        (period.start_time && period.end_time) &&
        this.isValidTimeDuration(period.start_time, period.end_time)
      );
    });

    if (!isAtLeastOneDayFilled) {
      this.notificationsService.showNotification('Please fill in at least one day with valid time duration (at least one hour).', 'danger');
      return;
    }

    this.toggle();

    this.scheduleService.updateSchedule(employeeId, schedule).subscribe(
      (data: any) => {
        if (data.id == this.employee.id) {
          this.employee = data;
          this.notificationsService.showNotification('Schedule updated successfully', 'success');
          this.router.navigate(this.path());
        } else {
          this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
          this.toggle();
        }
      },
      (_error: any) => {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
        this.toggle();
      }
    );
  }

  isValidTimeDuration(startTime: string, endTime: string): boolean {
    const startDateTime = new Date(`2000-01-01T${startTime}`);
    const endDateTime = new Date(`2000-01-01T${endTime}`);
    const durationInMinutes = (endDateTime.getTime() - startDateTime.getTime()) / (1000 * 60);
    return durationInMinutes >= 60;
  }

  path(): (string | number)[] {

    if (this.isNew) {
      return ['../../../', 'employees', 'list'];
    }

    return this.employee.id == this.currentEmployee.id ?
      ['../../../', 'my-profile', this.employee.id, 'details'] :
      ['../../../', 'employees', this.employee.id, 'details'];
  }

  goBack() {
    this.router.navigate(this.path());
  }

  canEdit() {
    return !this.isNew;
  }

  excludeRoles(roleList: Role[]) {
    this.validRoles = (this.employee.role.tier == Roles.Apprentice) ?
      roleList.filter((rol) => rol.tier !== Roles.Admin) :
      roleList.filter((rol) => rol.tier !== Roles.Apprentice);
  }

  onSelectedFile(uploadEvent: any) {
    var reader = new FileReader();
    reader.onload = (onLoadEvent: any) => {
      this.employee.profile = onLoadEvent.target.result;
    }
    reader.readAsDataURL(uploadEvent.target.files[0]);
    this.employee.attachment = <File>uploadEvent.target.files[0];
  }

  removeImage() {
    this.employee.profile = null;
    this.employee.attachment = null;
  }

  updateEmployeeKey() {
    if (this.currentEmployee.id === this.employee.id) {
      this.storage.setItem(EMPLOYEE_KEY, JSON.stringify(this.employee));
      this.navbarService.updateNavbar();
    }
  }

  updateEmployeeProfileImages(employee: Employee) {
    if (employee.attachment != this.currentEmployee.attachment || employee.profile != this.currentEmployee.profile) {
      this.toggle();
      this.employeesService.updateEmployeeProfileImages(employee).subscribe((data: any) => {
        if (data.id == this.employee.id) {
          this.employee = data;
          this.updateEmployeeKey();
          this.router.navigate(this.path());
          this.notificationsService.showNotification('Profile image updated successfully', 'success');
        } else {
          this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
          this.toggle();
        }
      }, (_error: any) => {
        this.notificationsService.showNotification('Upload an image.', 'danger');
        this.toggle();
      })
    }
  }

  openPasswordDialog(employee: Employee) {
    const dialogRef = this.dialog.open(ChangePasswordDialogComponent, {
      width: '45%',
      disableClose: true,
      data: employee,
    })
  }

  ngAfterViewInit(): void {
    this.route.params.subscribe(params => {
      if (params['change'] == 'picture') {
        this.moveStepper(2);
      }
    });
  }

  ngOnInit() {
    if (this.router.url == '/employees/new') {
      this.isNew = true;
      this.employee = new Employee();
      this.employee.active = true;
      this.employee.vacation_control = true;
      this.rolesService.getRoles().subscribe((data: any) => {
        this.validRoles = data;
      }, console.error);
      this.currentIdType = 'National'
      this.setCorrespodingIdNumberValue(this.currentIdType)
    } else {
      this.isNew = false;
      this.route.data.subscribe((data: { employee: any }) => {
        this.employee = data.employee;
        this.previousEmployee = this.employee;
        this.currentIdType = data.employee.id_number.length <= 15 ? 'National' : 'Resident'
        this.setCorrespodingIdNumberValue(this.currentIdType)
        this.employeeRole = this.employee.role;
        this.employeeHireDate = this.employee.hire_date;
        this.employee.schedule.work_days.forEach((day: any) => {
          day.periods.forEach((period: any) => {
            period._destroy = false;
          })
        });
      })
      this.rolesService.getRoles().subscribe((data: any) => {
        this.validRoles = data;
        this.excludeRoles(this.validRoles);
      }, console.error);
    }
    this.emailForm = this.fb.group({
      emailInput: ['', [Validators.required, this.validateEmail]]
    });
  }
}
