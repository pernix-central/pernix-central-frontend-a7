import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficialLinksComponent } from './official-links.component';

describe('OfficialLinksComponent', () => {
  let component: OfficialLinksComponent;
  let fixture: ComponentFixture<OfficialLinksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfficialLinksComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfficialLinksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
