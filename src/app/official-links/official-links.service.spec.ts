import { TestBed } from '@angular/core/testing';

import { OfficialLinksService } from './official-links.service';

describe('OfficialLinksService', () => {
  let service: OfficialLinksService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(OfficialLinksService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
