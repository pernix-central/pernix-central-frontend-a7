import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from '@components/components.module';

import { OfficialLinksComponent } from './official-links.component';
import { OfficialLinkListComponent } from './official-link-list/official-link-list.component';
import { OfficialLinkFormComponent } from './official-link-form/official-link-form.component';


@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [
    OfficialLinksComponent,
    OfficialLinkListComponent,
    OfficialLinkFormComponent
  ],
  providers: [],
  bootstrap: [OfficialLinksComponent]
})
export class OfficialLinksModule { }
