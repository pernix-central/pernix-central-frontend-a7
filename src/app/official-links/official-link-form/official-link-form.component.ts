import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { OfficialLinksService } from '../official-links.service';
import { NotificationsService } from '@services/notifications.service';
import { SweetAlertService } from '@services/sweet-alert.service';

import { OfficialLink } from '@models/official-link';
import { OfficialLinkData } from '@interfaces/official-link-data';
import { Status } from '@enums/status';

import Swal from 'sweetalert2';


@Component({
  selector: 'app-official-link-form',
  templateUrl: './official-link-form.component.html'
})
export class OfficialLinkFormComponent {
  officialLink: OfficialLink;
  isNew: boolean;
  isLoading: boolean;

  constructor(
    private officialLinkService: OfficialLinksService,
    private notificationsService: NotificationsService,
    private sweetAlertService: SweetAlertService,
    public dialogRef: MatDialogRef<OfficialLinkFormComponent>,
    @Inject(MAT_DIALOG_DATA) public data: OfficialLinkData
  ) {
    this.officialLink = data.officialLinkData;
    this.isNew = data.isNew;
  }

  validateHyperlinkFormat(hyperlink: string): boolean {
    let urlRegex = /^(?:http(s)?:\/\/)[\w.-]+(?:\.[\w\.-]+)+[\w\-\._~:/?#[\]@!\$&'\(\)\*\+,;=.]+$/;
    if (!urlRegex.test(hyperlink)) {
      this.notificationsService.showNotification('URL format is invalid!', 'danger');
      return false;
    }
    return true;
  }

  toggle() {
    if (this.isLoading) {
      this.isLoading = false;
    } else {
      this.isLoading = true;
    }
  }

  addOfficialLink(officialLink: OfficialLink) {
    if (this.validateHyperlinkFormat(officialLink.hyperlink)) {
      this.toggle();
      this.officialLinkService.createOfficialLink(officialLink).subscribe((data: any) => {
        if (data.name == officialLink.name) {
          this.notificationsService.showNotification('Official Link created successfully', 'success');
          this.dialogRef.close(true);
        } else {
          this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
          this.dialogRef.close(false);
        }
      }, (_error: any) => {
        this.notificationsService.showNotification('Please fill out the required fields', 'danger');
        this.toggle();
      })
    }
  }

  updateOfficialLink(officialLink: OfficialLink) {
    if (this.validateHyperlinkFormat(officialLink.hyperlink)) {
      this.toggle();
      this.officialLinkService.updateOfficialLink(officialLink).subscribe((data: any) => {
        if (data.name == officialLink.name) {
          this.notificationsService.showNotification('Official Link update successfully', 'success');
          this.dialogRef.close(true);
        } else {
          this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
          this.dialogRef.close(false);
        }
      }, (_error: any) => {
        this.notificationsService.showNotification('Please fill out the required fields', 'danger');
        this.toggle();
      })
    }
  }

  deleteOfficialLink(officialLink: OfficialLink) {
    this.sweetAlertService.showConfirmationDialog().fire({
      html: '<b>Are you sure you want to delete this official link?</b><br><br>This action CANNOT be undone!',
    }).then((result) => {
      if (result.isConfirmed) {
        this.officialLinkService.destroyOfficialLink(officialLink.id).subscribe((data: any) => {
          if (data == Status.OK) {
            this.notificationsService.showNotification('Official Link deleted successfully', 'success');
            this.dialogRef.close(true);
          } else {
            this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
            this.dialogRef.close(false);
          }
        })
      } else if (
        result.dismiss === this.sweetAlertService.showConfirmationDialog().DismissReason.cancel
      ) {
      }
    })
  }

  cancel() {
    this.dialogRef.close(false);
  }
}
