import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficialLinkFormComponent } from './official-link-form.component';

describe('OfficialLinkFormComponent', () => {
  let component: OfficialLinkFormComponent;
  let fixture: ComponentFixture<OfficialLinkFormComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OfficialLinkFormComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfficialLinkFormComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
