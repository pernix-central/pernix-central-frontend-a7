import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { Sort } from '@angular/material/sort';

import { OfficialLinksService } from '../official-links.service';
import { PermissionsService } from '@services/permissions.service';
import { NotificationsService } from '@services/notifications.service';

import { OfficialLink } from '@models/official-link';

import { OfficialLinkFormComponent } from '../official-link-form/official-link-form.component';

@Component({
  selector: 'app-official-link-list',
  templateUrl: './official-link-list.component.html'
})
export class OfficialLinkListComponent implements OnInit {
  officialLinks: OfficialLink[];
  sortedData: OfficialLink[];
  officialLink: OfficialLink;

  constructor(
    private officialLinksService: OfficialLinksService,
    private permissionsService: PermissionsService,
    private notificationsService: NotificationsService,
    private dialog: MatDialog
  ) { }

  canAccess() {
    return this.permissionsService.currentEmployeeIsAdmin()
  }

  openDialog(officialLink: OfficialLink, isNew: boolean) {
    const dialogRef = this.dialog.open(OfficialLinkFormComponent, {
      width: '50%',
      data: {
        officialLinkData: officialLink,
        isNew: isNew
      },
      disableClose: true,
      panelClass: 'mobile-dialog',
    })

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getOfficialLinks();
      }
    })
  }

  addOfficialLink() {
    this.officialLink = new OfficialLink();
    this.openDialog(this.officialLink, true);
  }

  updateOfficialLink(officialLinkId: number) {
    this.officialLinksService.getOfficialLink(officialLinkId).subscribe((data: any) => {
      this.officialLink = data;
      this.openDialog(this.officialLink, false);
    }, (_error: any) => {
      this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
    })
  }

  sortData(sort: Sort) {
    const data = this.officialLinks.slice();
    if (!sort.active || !sort.direction) {
      this.sortedData = data;
      return;
    }

    this.sortedData = data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'name': return this.compare(a.name, b.name, isAsc);
        default: return 0;
      }
    });
  }

  compare(a: number | string, b: number | string, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  getOfficialLinks() {
    this.sortedData = null;
    this.officialLinksService.getOfficialLinks().subscribe((data: any) => {
      this.officialLinks = data;
      this.sortedData = this.officialLinks.slice();
    }, (_error: any) => {
      this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
    })
  }

  ngOnInit(): void {
    this.getOfficialLinks();
  }
}
