import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OfficialLinkListComponent } from './official-link-list.component';

describe('OfficialLinkListComponent', () => {
  let component: OfficialLinkListComponent;
  let fixture: ComponentFixture<OfficialLinkListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [OfficialLinkListComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OfficialLinkListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
