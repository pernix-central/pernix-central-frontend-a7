import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '@services/storage.service';

import { OfficialLink } from '@models/official-link';
import { environment } from '@environments/environment';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class OfficialLinksService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  getOfficialLinks() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/official_links`;

    return this.http.get(url, this.httpOptions);
  }

  getOfficialLink(officialLinkId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/official_links/${officialLinkId}`;

    return this.http.get(url, this.httpOptions);
  }

  createOfficialLink(officialLink: OfficialLink) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/official_links`;

    let payload = {
      official_link: {
        name: officialLink.name,
        hyperlink: officialLink.hyperlink
      }
    };

    return this.http.post(url, payload, this.httpOptions);
  }

  updateOfficialLink(officialLink: OfficialLink) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/official_links/${officialLink.id}`;

    let payload = {
      official_link: {
        name: officialLink.name,
        hyperlink: officialLink.hyperlink
      }
    };

    return this.http.put(url, payload, this.httpOptions);
  }

  destroyOfficialLink(officialLinkId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/official_links/${officialLinkId}`;

    return this.http.delete(url, this.httpOptions);
  }
}
