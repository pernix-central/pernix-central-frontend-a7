import { Component } from '@angular/core';
import { MatDialogRef } from '@angular/material/dialog';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RecoveryService } from '@services/recovery.service';
import { NotificationsService } from "@services/notifications.service";

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.component.html'
})
export class PasswordRecoveryComponent {
  emailForm: FormGroup;
  errorMessage: string;
  isLoading: boolean;

  constructor(
    public dialogRef: MatDialogRef<PasswordRecoveryComponent>,
    private fb: FormBuilder,
    private notificationsService: NotificationsService,
    private recoveryService: RecoveryService,
  ) {
    this.isLoading = false;
    this.emailForm = this.fb.group({
      email: ['', [Validators.required, Validators.email, this.customEmailValidator]],
    });
  }

  close(): void {
    this.dialogRef.close();
  }

  sendRecoveryEmail(): void {
    if (this.emailForm.valid) {
      this.isLoading = true;
      const email = this.emailForm.value.email;
      this.recoveryService.sendRecoveryEmail(email).subscribe(
        (response) => {
          if (response.error) {
            this.errorMessage = response.error;
            this.isLoading = false;
          } else {
            this.notificationsService.showNotification(response.message, 'success');
            this.dialogRef.close();      
          }
        },
        (error) => {
          console.error('Error sending recovery email:', error);
          this.isLoading = false;
          this.errorMessage = 'An error occurred while sending the recovery email.';
        }
      );
    }
  }

  private customEmailValidator(control) {
    const email = control.value;
    if (email && !/(.*@pernixlabs\.com|.*@pernix-solutions\.com)$/.test(email)) {
      return { invalidEmail: true };
    }
    return null;
  }
}
