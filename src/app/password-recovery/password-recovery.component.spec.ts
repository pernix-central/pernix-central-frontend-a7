import { ComponentFixture, TestBed } from '@angular/core/testing';
import { FormBuilder, ReactiveFormsModule } from '@angular/forms';
import { MatDialogRef } from '@angular/material/dialog';
import { of, throwError } from 'rxjs';
import { PasswordRecoveryComponent } from './password-recovery.component';
import { RecoveryService } from '../services/recovery.service';

describe('PasswordRecoveryComponent', () => {
  let component: PasswordRecoveryComponent;
  let fixture: ComponentFixture<PasswordRecoveryComponent>;
  let dialogRefSpy: jasmine.SpyObj<MatDialogRef<PasswordRecoveryComponent>>;
  let recoveryServiceSpy: jasmine.SpyObj<RecoveryService>;

  beforeEach(() => {
    const dialogRefSpyObj = jasmine.createSpyObj('MatDialogRef', ['close']);
    const recoveryServiceSpyObj = jasmine.createSpyObj('RecoveryService', ['sendRecoveryEmail']);

    TestBed.configureTestingModule({
      declarations: [PasswordRecoveryComponent],
      imports: [ReactiveFormsModule],
      providers: [
        { provide: MatDialogRef, useValue: dialogRefSpyObj },
        { provide: RecoveryService, useValue: recoveryServiceSpyObj },
        FormBuilder,
      ],
    });

    fixture = TestBed.createComponent(PasswordRecoveryComponent);
    component = fixture.componentInstance;
    dialogRefSpy = TestBed.inject(MatDialogRef) as jasmine.SpyObj<MatDialogRef<PasswordRecoveryComponent>>;
    recoveryServiceSpy = TestBed.inject(RecoveryService) as jasmine.SpyObj<RecoveryService>;
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should close dialog on closeDialog()', () => {
    component.close();
    expect(dialogRefSpy.close).toHaveBeenCalled();
  });

  it('should send recovery email and handle success', () => {
    const email = 'test@example.com';
    const response = { message: 'Recovery email sent successfully' };

    recoveryServiceSpy.sendRecoveryEmail.and.returnValue(of(response));

    component.emailForm.setValue({ email });
    component.sendRecoveryEmail();

    expect(recoveryServiceSpy.sendRecoveryEmail).toHaveBeenCalledWith(email);
    expect(component.errorMessage).toBeNull();
  });

  it('should handle error when sending recovery email fails', () => {
    const email = 'test@example.com';
    const errorResponse = 'Error sending recovery email';

    recoveryServiceSpy.sendRecoveryEmail.and.returnValue(throwError(errorResponse));

    component.emailForm.setValue({ email });
    component.sendRecoveryEmail();

    expect(recoveryServiceSpy.sendRecoveryEmail).toHaveBeenCalledWith(email);
    expect(component.errorMessage).toBe('An error occurred while sending the recovery email.');
  });

  it('should validate custom email domain', () => {
    // Valid email domain
    component.emailForm.setValue({ email: 'test@pernixlabs.com' });
    expect(component.emailForm.valid).toBe(true);

    // Invalid email domain
    component.emailForm.setValue({ email: 'test@example.com' });
    expect(component.emailForm.hasError('invalidEmail')).toBe(true);
  });
});
