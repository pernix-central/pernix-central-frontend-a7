import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { JwtHelperService, JwtModule } from '@auth0/angular-jwt';

import { LoginComponent } from '../login/login.component';

import { ChecklistComponent } from '../checklist/checklist.component';
import { ChecklistListComponent } from '../checklist/checklist-list/checklist-list.component';

import { EmployeesComponent } from '../employees/employees.component';
import { EmployeeListComponent } from '../employees/employee-list/employee-list.component';
import { EmployeeDetailsComponent } from '../employees/employee-details/employee-details.component';
import { EmployeeFormComponent } from '../employees/employee-form/employee-form.component';
import { EmployeeResolver } from '../employees/employee-resolver.service';

import { VacationsComponent } from '../vacations/vacations.component';
import { VacationsHistoryComponent } from '../vacations/vacations-history/vacations-history.component';

import { NotificationMessagesComponent } from '../notification-messages/notification-messages.component';
import { NotificationSettingsComponent } from '../notification-messages/notification-settings/notification-settings.component';

import { SuggestionsComponent } from '../suggestions/suggestions.component';
import { SuggestionListComponent } from '../suggestions/suggestion-list/suggestion-list.component';
import { SuggestionDetailsComponent } from '../suggestions/suggestion-details/suggestion-details.component';
import { SuggestionResolverService } from '../suggestions/suggestion-resolver.service';

import { OfficialLinksComponent } from '../official-links/official-links.component';
import { OfficialLinkListComponent } from '../official-links/official-link-list/official-link-list.component';

import { WalletComponent } from '../wallet/wallet.component';
import { WalletHistoryComponent } from '../wallet/wallet-history/wallet-history.component';
import { WalletReviewSummaryComponent } from '../wallet/wallet_review_summary/wallet_review_summary.component';

import { AuthGuard } from '../guards/auth-guard.service';
import { RoleGuard } from '../guards/role-guard.service';

import { ResetPasswordComponent } from '../reset-password/reset-password.component';


const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponent },
  { path: 'home', redirectTo: '/home/checklist', pathMatch: 'full' },
  { path: 'employees', redirectTo: '/employees/list', pathMatch: 'full' },
  { path: 'suggestions', redirectTo: '/suggestions/list', pathMatch: 'full' },
  { path: 'vacations', redirectTo: '/vacations/history', pathMatch: 'full' },
  { path: 'admin/notifications', redirectTo: '/admin/notifications/list', pathMatch: 'full' },
  { path: 'official-links', redirectTo: '/official-links/list', pathMatch: 'full' },
  { path: 'my-profile/:employee_id', redirectTo: 'my-profile/:employee_id/details', pathMatch: 'full' },
  { path: 'notifications', redirectTo: '/notifications/settings', pathMatch: 'full' },
  { path: 'wallet', redirectTo: '/wallet/history', pathMatch: 'full' },
  { path: 'reset-password/:token', component: ResetPasswordComponent },
  {
    path: 'home',
    component: ChecklistComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'checklist',
        component: ChecklistListComponent
      }
    ]
  },
  {
    path: 'employees',
    component: EmployeesComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'list',
        component: EmployeeListComponent
      },
      {
        path: ':employee_id/details',
        component: EmployeeDetailsComponent,
        resolve: {
          employee: EmployeeResolver
        }
      },
      {
        path: 'new',
        component: EmployeeFormComponent,
        canActivate: [RoleGuard]
      },
      {
        path: ':employee_id/edit',
        component: EmployeeFormComponent,
        resolve: {
          employee: EmployeeResolver
        },
        canActivate: [RoleGuard]
      },
    ]
  },
  {
    path: 'my-profile',
    component: EmployeesComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: ':employee_id/details',
        component: EmployeeDetailsComponent,
        resolve: {
          employee: EmployeeResolver
        }
      },
      {
        path: ':employee_id/edit',
        component: EmployeeFormComponent,
        resolve: {
          employee: EmployeeResolver
        },
        canActivate: [RoleGuard]
      }
    ]
  },
  {
    path: 'notifications',
    component: NotificationMessagesComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'settings',
        component: NotificationSettingsComponent,
      }
    ]
  },
  {
    path: 'suggestions',
    component: SuggestionsComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'list',
        component: SuggestionListComponent
      },
      {
        path: ':suggestion_id/details',
        component: SuggestionDetailsComponent,
        resolve: {
          suggestion: SuggestionResolverService
        }
      }
    ]
  },
  {
    path: 'vacations',
    component: VacationsComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'history',
        component: VacationsHistoryComponent,
        canActivate: [RoleGuard]
      },
      {
        path: ':employee_id/vacation_history',
        component: VacationsHistoryComponent,
        resolve: {
          employee: EmployeeResolver
        },
        canActivate: [RoleGuard]
      }
    ]
  },
  {
    path: 'wallet',
    component: WalletComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'history',
        component: WalletHistoryComponent,
      },
      {
        path: ':employee_id/wallet_history',
        component: WalletHistoryComponent,
        resolve: {
          employee: EmployeeResolver
        },
        canActivate: [RoleGuard]
      },
      {
        path: 'summary',
        component: WalletReviewSummaryComponent,
        canActivate: [RoleGuard]
      }
    ]
  },
  {
    path: 'official-links',
    component: OfficialLinksComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'list',
        component: OfficialLinkListComponent
      }
    ]
  }
];

export function getToken(): string {
  return '';
}

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes),
    JwtModule.forRoot({
      config: {
        tokenGetter: getToken
      }
    })
  ],
  exports: [RouterModule],
  providers: [
    JwtHelperService,
  ]
})
export class RoutesModule { }
