import { Injectable } from '@angular/core';

export abstract class StorageService {
  public abstract get(): Storage;
}

@Injectable()
export class LocalStorage extends StorageService {
  public get(): Storage {
    return localStorage;
  }
}
