import { TestBed } from '@angular/core/testing';

import { EvaluationPeriodsService } from './evaluation-periods.service';

describe('EvaluationPeriodsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: EvaluationPeriodsService = TestBed.get(EvaluationPeriodsService);
    expect(service).toBeTruthy();
  });
});
