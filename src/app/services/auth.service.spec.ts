// auth.service.spec.ts
import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { AuthService } from './auth.service';

class MockAuthService extends AuthService {

  getApiUrl() {
    return this.apiUrl;
  }
}

describe('AuthService', () => {
  let authService: MockAuthService;  
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MockAuthService], 
    });

    authService = TestBed.inject(MockAuthService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(authService).toBeTruthy();
  });

  describe('resetPassword', () => {
    it('should send a POST request with the correct URL and payload', () => {
      // Use getApiUrl() to access the protected property
      const apiUrl = authService.getApiUrl();
      const resetToken = 'test-reset-token';
      const newPassword = 'test-new-password';

      authService.resetPassword(resetToken, newPassword).subscribe();

      const req = httpTestingController.expectOne(request => {
        return (
          request.method === 'POST' &&
          request.url === `${apiUrl}/v1/employees/update_password` &&
          JSON.stringify(request.body) === JSON.stringify({ reset_token: resetToken, new_password: newPassword })
        );
      });

      req.flush({}); 
    });

  });
});
