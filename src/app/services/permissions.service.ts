import { Injectable } from '@angular/core';

import { StorageService } from '@services/storage.service';

import { Employee } from '@models/employee';
import { environment } from '@environments/environment';

const EMPLOYEE_KEY = environment.employee_key;
const ROLES = environment.roles;

@Injectable({
  providedIn: 'root'
})
export class PermissionsService {
  private storage: Storage;
  private adminTier: Number;
  private crafterTier: Number;
  private apprenticeTier: Number;

  constructor(
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
    this.adminTier = ROLES.find(r => r.tier == 0).tier;
    this.crafterTier = ROLES.find(r => r.tier == 1).tier;
    this.apprenticeTier = ROLES.find(r => r.tier == 2).tier;
  }

  currentEmployeeIsAdmin() {
    let currentUser: Employee;
    currentUser = JSON.parse(this.storage.getItem(EMPLOYEE_KEY));

    return currentUser?.role?.tier == this.adminTier;
  }

  currentEmployeeIsCrafter() {
    let currentUser: Employee;
    currentUser = JSON.parse(this.storage.getItem(EMPLOYEE_KEY));

    return currentUser?.role?.tier == this.crafterTier;
  }

  currentEmployeeIsApprentice() {
    let currentUser: Employee;
    currentUser = JSON.parse(this.storage.getItem(EMPLOYEE_KEY));

    return currentUser?.role?.tier == this.apprenticeTier;
  }

  vacationControlIsActive() {
    let currentUser: Employee;
    currentUser = JSON.parse(this.storage.getItem(EMPLOYEE_KEY));

    return currentUser?.vacation_control;
  }

  employeeIsApprentice(employee: Employee) {
    if (employee.role == undefined) {
      return false;
    } else {
      return employee.role.tier == this.apprenticeTier;
    }
  }

  isCurrentEmployee(employee: Employee) {
    let currentUser: Employee;
    currentUser = JSON.parse(this.storage.getItem(EMPLOYEE_KEY));

    return currentUser?.id == employee.id;
  }

  isCurrentEmployeeId(employeeId: number) {
    let currentUser: Employee;
    currentUser = JSON.parse(this.storage.getItem(EMPLOYEE_KEY));

    return currentUser?.id == employeeId;
  }

  employeeIsAdmin(employee: Employee) {
    if (employee.role == undefined) return false;

    return employee.role.tier == this.adminTier;
  }

}
