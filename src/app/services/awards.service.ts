import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '@services/storage.service';

import { environment } from '@environments/environment';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class AwardsService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  getAwards() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const url = `${this.apiUrl}/v1/awards`;

    return this.http.get(url, this.httpOptions)
  }
}
