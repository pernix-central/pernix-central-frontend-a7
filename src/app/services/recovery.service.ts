// recovery.service.ts
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '@environments/environment';

@Injectable({
  providedIn: 'root',
})
export class RecoveryService {
  protected apiUrl = environment.apiUrl;
  protected httpOptions: any;

  constructor(private http: HttpClient) { }


  getApiUrl() {
    return this.apiUrl;
  }

  getHttpOptions() {
    return this.httpOptions;
  }

  sendRecoveryEmail(email: string): Observable<any>{
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };

    const url = `${this.apiUrl}/v1/employees/password_recovery`;
    const recoveryData = { email: email };

    return this.http.post(url, recoveryData, this.httpOptions).pipe(
      catchError((error) => {
        console.error('Error sending recovery email:', error);
        const errorMessage = error.error && error.error.error_message ? error.error.error_message : 'An error occurred while sending the recovery email.';
        return of({ error: errorMessage });
      })
    );
  }
}
