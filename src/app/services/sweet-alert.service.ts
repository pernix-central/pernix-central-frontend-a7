import { Injectable } from '@angular/core';
import Swal from 'sweetalert2';

@Injectable({
  providedIn: 'root'
})
export class SweetAlertService {
  showConfirmationDialog() {
    return Swal.mixin({
      title: 'Confirmation dialog',
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger',
      },
      buttonsStyling: false,
      showCancelButton: true,
      confirmButtonText: 'Yes',
      cancelButtonText: 'No',
      reverseButtons: true,
    });
  };

  showInformationDialog() {
    return Swal.mixin({
      title: 'Information dialog',
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger',
      },
      showCancelButton: false,
      confirmButtonText: 'Ok',
      reverseButtons: true
    });
  };
}
