// auth.service.ts
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '@environments/environment';


@Injectable({
  providedIn: 'root',
})
export class AuthService {
  protected apiUrl = environment.apiUrl;
  private httpOptions: any;
  
  constructor(private http: HttpClient) {}
  
  resetPassword(reset_token: string, new_password: string, confirm_password: string): Observable<any> {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      })
    };

    const url = `${this.apiUrl}/v1/employees/update_password`;
    const body = { reset_token, new_password, confirm_password };
    return this.http.post(url, body, this.httpOptions);
  }
}
