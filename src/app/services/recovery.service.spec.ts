// recovery.service.spec.ts
import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { RecoveryService } from './recovery.service';

class MockRecoveryService extends RecoveryService {
  // Accessor methods for testing
  getApiUrl() {
    return super.getApiUrl();
  }

  getHttpOptions() {
    return super.getHttpOptions();
  }
}

describe('RecoveryService', () => {
  let recoveryService: MockRecoveryService; // Use the mock service
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [MockRecoveryService], // Use the mock service
    });

    recoveryService = TestBed.inject(MockRecoveryService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should be created', () => {
    expect(recoveryService).toBeTruthy();
  });

  describe('sendRecoveryEmail', () => {
    // ... Previous test cases ...

    it('should send a POST request with the correct URL and payload', () => {
      const apiUrl = recoveryService.getApiUrl();
      const httpOptions = recoveryService.getHttpOptions();
      const email = 'test@example.com';

      recoveryService.sendRecoveryEmail(email).subscribe();

      const req = httpTestingController.expectOne(request => {
        return (
          request.method === 'POST' &&
          request.url === `${apiUrl}/v1/employees/password_recovery` &&
          JSON.stringify(request.body) === JSON.stringify({ email: email }) &&
          request.headers.get('Content-Type') === httpOptions.headers.get('Content-Type')
        );
      });

      req.flush({}); // You can provide a mock response if needed
    });

    // Additional test cases...
  });
});
