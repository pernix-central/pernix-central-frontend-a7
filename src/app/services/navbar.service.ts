import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NavbarService {

  private update: boolean;
  private update$: Subject<boolean>;

  constructor(
  ) {
    this.update = true;
    this.update$ = new Subject();
  }

  updateNavbar() {
    this.update$.next(this.update);
  }

  getUpdate$(): Observable<boolean> {
    return this.update$.asObservable();
  }
}
