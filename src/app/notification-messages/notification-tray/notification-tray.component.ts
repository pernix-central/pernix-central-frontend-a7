import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { Router } from '@angular/router';

import { NotificationMessagesService } from '../notification-messages.service';

import { NotificationMessage } from '@models/notification-message';
import { NotificationMessageData } from '@interfaces/notification-message-data';

import * as moment from 'moment';

@Component({
  selector: 'app-notification-tray',
  templateUrl: './notification-tray.component.html'
})
export class NotificationTrayComponent {
  notificationMessages: NotificationMessage[];

  constructor(
    private notificationMessagesService: NotificationMessagesService,
    private router: Router,
    public dialogRef: MatDialogRef<NotificationTrayComponent>,
    @Inject(MAT_DIALOG_DATA) public data: NotificationMessageData
  ) {
    this.notificationMessages = data.notificationMessageData;
  }

  thereIsUnread(): boolean {
    return this.notificationMessages.filter(data => data.read == false).length > 0 ? true : false;
  }

  convertDate(date: string) {
    return moment(date).locale('en').fromNow();
  }

  readAllNotifications() {
    this.notificationMessagesService.markAllRead().subscribe(() => {
      this.dialogRef.close({ readAll: true });
    });
  }

  goToNotification(notification: NotificationMessage) {
    this.notificationMessagesService.markRead(notification.id).subscribe(() => {
      if (!notification.read) {
        this.dialogRef.close({
          readNotification: true,
          notificationType: notification.notification_type,
          senderId: notification.sender_id,
          extraInfo: notification.extra_information,
          notifyableId: notification.notifyable_id,
        });
      } else {
        this.dialogRef.close({
          readNotification: false,
          notificationType: notification.notification_type,
          senderId: notification.sender_id,
          extraInfo: notification.extra_information,
          notifyableId: notification.notifyable_id,
        });
      }
    });
  }

  refreshNotificationList(notifications:NotificationMessage[]) {
    this.notificationMessages = notifications;
  }

  goToSettings() {
    this.dialogRef.close();
    this.router.navigate(['notifications', 'settings']);
  }

}
