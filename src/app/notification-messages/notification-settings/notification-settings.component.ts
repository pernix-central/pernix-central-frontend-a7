import { Component, OnInit } from '@angular/core';

import { NotificationMessagesService } from '../notification-messages.service';
import { NotificationsService } from '@services/notifications.service';
import { PermissionsService } from '@services/permissions.service';

import { NotificationSettings } from '@models/notification-settings';

@Component({
  selector: 'app-notification-settings',
  templateUrl: './notification-settings.component.html'
})
export class NotificationSettingsComponent implements OnInit {
  settings: NotificationSettings;

  constructor(
    private notificationMessagesService: NotificationMessagesService,
    private notificationsService: NotificationsService,
    private permissionsService: PermissionsService,
  ) { }

  currentEmployeeIsAdmin() {
    return this.permissionsService.currentEmployeeIsAdmin();
  }

  currentEmployeeIsApprentice() {
    return this.permissionsService.currentEmployeeIsApprentice();
  }

  updateSettings() {
    this.notificationMessagesService.updateNotificationSettings(this.settings).subscribe((data: any) => {
      if (data.id == this.settings.id) {
        this.getSettings();
        this.notificationsService.showNotification('Settings updated successfully', 'success');
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      }
    });
  }

  getSettings() {
    this.notificationMessagesService.getNotificationSettings().subscribe((data: any) => {
      this.settings = data;
    });
  }

  ngOnInit(): void {
    this.getSettings();
  }

}
