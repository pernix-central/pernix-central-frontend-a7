import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ComponentsModule } from '@components/components.module';

import { NotificationTrayComponent } from './notification-tray/notification-tray.component';
import { NotificationMessagesComponent } from './notification-messages.component';
import { NotificationSettingsComponent } from './notification-settings/notification-settings.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule
  ],
  declarations: [
    NotificationMessagesComponent,
    NotificationTrayComponent,
    NotificationSettingsComponent
  ],
  providers: [],
  bootstrap: [NotificationMessagesComponent]
})
export class NotificationsMessagesModule { }
