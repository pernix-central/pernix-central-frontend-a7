import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '@services/storage.service';

import { NotificationSettings } from '@models/notification-settings';
import { environment } from '@environments/environment';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class NotificationMessagesService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  getNotificationSettings() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/notification_settings/show_employee_settings`;

    return this.http.get(url, this.httpOptions);
  }

  updateNotificationSettings(settings: NotificationSettings) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/notification_settings/${settings.id}`;

    let payload = {
      notification_setting: {
        anniversary:
        {
          "email": settings.anniversary.email,
          "tray": settings.anniversary.tray,
        },
        birthday:
        {
          "email": settings.birthday.email,
          "tray": settings.birthday.tray,
        },
        change_role:
        {
          "email": settings.change_role.email,
          "tray": settings.change_role.tray,
        },
        vacation_request_reviewed:
        {
          "email": settings.vacation_request_reviewed.email,
          "tray": settings.vacation_request_reviewed.tray,
        },
        vacation_request_submitted:
        {
          "email": settings.vacation_request_submitted.email,
          "tray": settings.vacation_request_submitted.tray,
        },
        vacation_request_reminder:
        {
          "email": settings.vacation_request_reminder.email,
          "tray": settings.vacation_request_reminder.tray,
          "notify_on": settings.vacation_request_reminder.notify_on
        },
        wallet_request_reminder:
        {
          "email": settings.wallet_request_reminder.email,
          "tray": settings.wallet_request_reminder.tray,
          "notify_on": settings.wallet_request_reminder.notify_on
        },
        wallet_request_reviewed: 
        {
          "email": settings.wallet_request_reviewed.email, 
          "tray": settings.wallet_request_reviewed.tray,
        },
        suggestion_responses:
        {
          "email": settings.suggestion_responses.email,
          "tray": settings.suggestion_responses.tray,
        },
        change_schedule:
        {
          "email": settings.change_schedule.email,
          "tray": settings.change_schedule.tray,
        },
        request_removal:
        {
          "email": settings.request_removal.email,
          "tray": settings.request_removal.tray,
        }
      },
    };

    return this.http.put(url, payload, this.httpOptions);
  }

  getNotificationMessages() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/notification_employees/notifications_messages`;

    return this.http.get(url, this.httpOptions);
  }

  markAllRead() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/notification_employees/update_all`;

    return this.http.get(url, this.httpOptions);
  }

  markRead(notificationId: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/notification_employees/${notificationId}`;

    return this.http.put(url, notificationId, this.httpOptions);
  }

}