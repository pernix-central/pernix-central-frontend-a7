// reset-password.component.ts

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthService } from '../services/auth.service';
import { NotificationsService } from '@services/notifications.service';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
})
export class ResetPasswordComponent implements OnInit {
  resetToken: string;
  newPassword: string = '';
  confirmPassword: string = '';
  loading: boolean;

  constructor(
    private route: ActivatedRoute,
    private authService: AuthService,
    private router: Router,
    private notificationsService: NotificationsService
  ) { }

  ngOnInit(): void {
    this.loading = true;
    this.route.params.subscribe((params) => {
      this.resetToken = params['token'];
    });
    this.loading = false;
  }

  resetPassword(): void {
    if (this.newPassword !== this.confirmPassword) {
      this.notificationsService.showNotification('Passwords do not match', 'danger');
      return;
    }

    this.authService.resetPassword(this.resetToken, this.newPassword, this.confirmPassword).subscribe(
      (response) => {
        this.loading = true;
        const resetMessage = response.message;
        if (resetMessage === 'Password Updated.') {
          this.loading = false;
          this.notificationsService.showNotification('Password Updated Successfully.', 'success');
          this.router.navigate(['/login'], { state: { resetMessage } });
        }
      },
      (error) => {
        this.loading = false;
        this.notificationsService.showNotification('Error resetting password. Please try again.', 'danger')        
      }
    );
  }
}
