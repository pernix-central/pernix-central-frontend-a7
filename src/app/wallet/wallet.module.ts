import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';

import { ComponentsModule } from '@components/components.module';

import { WalletComponent } from './wallet.component';
import { WalletHistoryComponent } from './wallet-history/wallet-history.component';
import { WalletRequestComponent } from './wallet-request/wallet-request.component';
import { WalletRequestDetailsComponent } from './wallet-request-details/wallet-request-details.component';
import { WalletReviewSummaryComponent } from './wallet_review_summary/wallet_review_summary.component';

@NgModule({
  imports: [
    CommonModule,
    ComponentsModule,
    MatFormFieldModule,
    MatInputModule
  ],
  declarations: [
    WalletComponent,
    WalletHistoryComponent,
    WalletRequestComponent,
    WalletRequestDetailsComponent,
    WalletReviewSummaryComponent,
  ],
  providers: [],
  bootstrap: [WalletComponent]
})
export class WalletModule { }
