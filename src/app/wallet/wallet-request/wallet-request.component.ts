import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { WalletService } from '../wallet.service';
import { AwardsService } from '@services/awards.service';
import { NotificationsService } from '@services/notifications.service';
import { SweetAlertService } from '@services/sweet-alert.service';

import { Employee } from '@models/employee';
import { WalletRequest } from '@models/wallet-request';
import { Award } from '@models/award';

import { environment } from '@environments/environment';
import { WalletRequestData } from '@interfaces/wallet-data';
import { RequestType } from '@enums/request-type';

const EMPLOYEE_KEY = environment.employee_key;

@Component({
  selector: 'app-wallet-request',
  templateUrl: './wallet-request.component.html',
})
export class WalletRequestComponent {
  currentUser: Employee = JSON.parse(localStorage.getItem(EMPLOYEE_KEY));
  walletRequest: WalletRequest = new WalletRequest();
  availablePoints: number = this.currentUser.my_way_points_available;
  isLoading: boolean;
  requestType = RequestType;
  awardList: Award[] = [];
  formattedAwardList: string[] = [];
  filteredAwardList: string[] = [];
  selectedAward: string;
  writtenAward: string;
  eventDay: string;
  dateList: Date[] = [];

  constructor(
    private walletRequestService: WalletService,
    private awardsService: AwardsService,
    private notificationsService: NotificationsService,
    private sweetAlertService: SweetAlertService,
    public dialogRef: MatDialogRef<WalletRequestComponent>,
    @Inject(MAT_DIALOG_DATA) public data: WalletRequestData
  ) {
    if (data.request_type === RequestType.CREDIT) {
      this.getAwards();
      this.fillDates();
    }
  }

  toggle() {
    this.isLoading = !this.isLoading;
  }

  setSelectedAward(award: string) {
    this.selectedAward = award;
  }

  getAwards() {
    this.awardsService.getAwards().subscribe((data: any) => {
      this.awardList = data;
      this.formattedAwardList = this.awardList.map(award => `${award.name} - ${award.points} pts`);
      this.filteredAwardList = this.formattedAwardList;
    }, console.error);
  }

  validateFields() {
    const isCredit = this.data.request_type === RequestType.CREDIT;
    const isDebit = this.data.request_type === RequestType.DEBIT;

    const missingCreditFields = isCredit && (!this.writtenAward || !this.walletRequest.request_comments || !this.eventDay);
    const missingDebitFields = isDebit && (!this.walletRequest.request_name || !this.walletRequest.request_points || !this.walletRequest.request_comments);
    
    if (missingCreditFields || missingDebitFields) { 
      this.notificationsService.showNotification('Please fill out the required fields', 'danger');
    } else if (isCredit && this.writtenAward !== this.selectedAward) {
      this.notificationsService.showNotification('Please select the activity', 'danger');
    } else if (this.walletRequest.request_comments.length < 10) {
      this.notificationsService.showNotification('Please write a longer comment (minimum 10 characters)', 'danger');
    } else if (isDebit && this.walletRequest.request_points > this.currentUser.my_way_points_available) {
      this.notificationsService.showNotification('Your requested points exceed your available points.', 'warning');
    } else {
      this.sweetAlertService.showConfirmationDialog().fire({
        html: '<b>Are you sure you want to continue?</b>' +
          '<br><br>Your request will be reviewed by an administrator.',
      }).then((result) => {
        if (result.isConfirmed) {
          this.sendWalletRequest();
        }
      });
    }
  }

  fillDates() {
    const currentDate: Date = new Date();
    const yesterday = new Date(currentDate);
    yesterday.setDate(currentDate.getDate() - 1);
    const dayBeforeYesterday = new Date(currentDate);
    dayBeforeYesterday.setDate(currentDate.getDate() - 2);

    this.dateList.push(currentDate, yesterday, dayBeforeYesterday);

    if (currentDate.getDay() <= 2) {
      for (let i = 0; i < 2; i++) {
        const extraDate = new Date(currentDate);
        extraDate.setDate(currentDate.getDate() - (3 + i));
        this.dateList.push(extraDate);
      }
    }
  }

  formatDate(date: Date): string {
    const options: Intl.DateTimeFormatOptions = { weekday: 'long', day: 'numeric', month: 'short', year: 'numeric' };
    return date.toLocaleDateString('en-US', options);
  }

  filterList(searchTarget: string) {
    this.filteredAwardList = this.formattedAwardList.filter(award => award.toLowerCase().includes(searchTarget.toLowerCase()));
  }

  sendWalletRequest() {
    this.toggle();
    if (this.data.request_type === RequestType.CREDIT){
      const award = this.selectedAward.split(' - ');
      this.walletRequest.request_name = `${award[0]} - ${award[1]}`;
      this.walletRequest.request_points = parseInt(award[2].replace(' pts', ''));
    }
    this.walletRequest.employee_id = this.currentUser.id;
    this.walletRequest.request_type = this.data.request_type;
    this.walletRequestService.createWalletRequest(this.walletRequest).subscribe((data: any) => {
      if (data.employee_id === this.walletRequest.employee_id) {
        this.notificationsService.showNotification(`The ${this.data.request_type} request has been successfully sent`, 'success');
        if (this.data.request_type == RequestType.DEBIT) {
          this.currentUser.my_way_points_available -= this.walletRequest.request_points;
          localStorage.setItem(EMPLOYEE_KEY, JSON.stringify(this.currentUser));
        }
        this.toggle();
        this.dialogRef.close(true);
      } else {
        this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
        this.dialogRef.close(true);
      }
    })
  }

  cancel() {
    if (!!this.walletRequest.request_name || !!this.walletRequest.request_comments || !!this.walletRequest.request_points) {
      this.sweetAlertService.showConfirmationDialog().fire({
        html: '<b>Are you sure you want to exit?</b><br><br>Any changes made will be lost!',
      }).then((result) => {
        if (result.isConfirmed) {
          this.notificationsService.showNotification('Wallet request canceled', 'warning');
          this.dialogRef.close();
        } else {
          result.dismiss === this.sweetAlertService.showConfirmationDialog().DismissReason.cancel
        }
      })
    } else {
      this.notificationsService.showNotification('Wallet request canceled', 'warning');
      this.dialogRef.close();
    }
  }
}
