import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WalletRequestDetailsComponent } from './wallet-request-details.component';

describe('WalletRequestDetailsComponent', () => {
  let component: WalletRequestDetailsComponent;
  let fixture: ComponentFixture<WalletRequestDetailsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WalletRequestDetailsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WalletRequestDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
