import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';

import { WalletService } from '../wallet.service';
import { EmployeesService } from '../../employees/employees.service';
import { PermissionsService } from '@services/permissions.service';
import { NotificationsService } from '@services/notifications.service';
import { SweetAlertService } from '@services/sweet-alert.service';

import { Employee } from '@models/employee';
import { WalletRequest } from '@models/wallet-request';

import { environment } from '@environments/environment';
import { RequestStatus } from '@enums/request-status';
import { WalletRequestData } from '@interfaces/wallet-data';

import { Status } from '@enums/status';
import { Router } from '@angular/router';

const EMPLOYEE_KEY = environment.employee_key;

@Component({
  selector: 'app-wallet-request-details',
  templateUrl: './wallet-request-details.component.html',
})
export class WalletRequestDetailsComponent {
  walletRequest: WalletRequest;
  currentUserIsAdmin: boolean;
  statusIsPending: boolean;
  statusIsApproved: boolean;
  statusIsCancelled: boolean;
  currentEmployee: Employee = JSON.parse(localStorage.getItem(EMPLOYEE_KEY));
  employee: Employee;
  walletRequestStatus: string;
  requestStatus = RequestStatus;
  isLoading: boolean;

  constructor(
    private permissionsService: PermissionsService,
    private walletService: WalletService,
    private employeeService: EmployeesService,
    private notificationsService: NotificationsService,
    private sweetAlertService: SweetAlertService,
    private router: Router,
    public dialogRef: MatDialogRef<WalletRequestDetailsComponent>,
    @Inject(MAT_DIALOG_DATA) public data: WalletRequestData
  ) {
    this.walletRequest = data.walletRequestData;
    this.walletRequestStatus = this.walletRequest.status;
    this.currentUserIsAdmin = this.permissionsService.currentEmployeeIsAdmin();
    this.statusIsPending = this.isPending();
    this.statusIsApproved = this.isApproved();
    this.statusIsCancelled = this.isCancelled();
    this.getEmployee();
  }

  getEmployee() {
    this.employeeService.getEmployee(this.walletRequest.employee_id).
      subscribe((data: any) => { this.employee = data; });
  }

  isPending() {
    return this.walletRequest.status == RequestStatus.Pending;
  }

  isApproved() {
    return this.walletRequest.status == RequestStatus.Approved;
  }

  isCancelled() {
    return this.walletRequest.status == RequestStatus.Cancelled;
  }

  statusAllowedSave() {
    return this.isPending() || this.isApproved();
  }

  showStatus() {
    return !this.currentUserIsAdmin || this.walletRequest.status == RequestStatus.Denied || this.walletRequest.status == RequestStatus.Cancelled
  }

  toggle() {
    this.isLoading = !this.isLoading;
  }

  employeeIsCurrentEmployee() {
    return (this.currentEmployee.id != this.walletRequest.employee_id);
  }

  updateWalletRequest() {
    this.walletRequest.reviewer_id = this.currentEmployee.id;
    this.walletRequest.review_date = new Date().toISOString();
    if (this.walletRequestStatus == RequestStatus.Approve) {
      this.walletRequest.status = RequestStatus.Approved;
    } else if (this.walletRequestStatus == RequestStatus.Deny) {
      this.walletRequest.status = RequestStatus.Denied;
    } else {
      this.walletRequest.status = RequestStatus.Cancelled;
    }
  }

  save() {
    if (this.walletRequest.status != this.walletRequestStatus) {
      this.sweetAlertService.showConfirmationDialog().fire({
        html: `<b>Are you sure you want to "${this.walletRequestStatus}" the points?</b>
        <br><br>This action CANNOT be undone!`,
      }).then((result) => {
        if (result.isConfirmed) {
          this.toggle();
          this.updateWalletRequest();
          this.walletService.updateWalletRequest(this.walletRequest).subscribe((data: any) => {
            this.walletRequest = data;
            this.statusIsPending = false;
            this.notificationsService.showNotification('Your changes have been saved', 'success');
            this.dialogRef.close(true);
          }, (_error: any) => {
            this.walletRequest.reviewer_name = null;
            this.walletRequestStatus = this.walletRequest.status;
            this.toggle();
          },
          );
          this.clearPath();
        }
      })
    } else {
      this.dialogRef.close();
      this.clearPath();
    }
  }

  deleteWalletRequest(event: any, id: number) {
    event.stopPropagation();

    this.sweetAlertService.showConfirmationDialog().fire({
      html: '<b>Are you sure you want to remove this wallet request?</b>',
    }).then((result) => {
      if (result.isConfirmed) {
        this.toggle();
        this.walletService.deleteWalletRequest(id).subscribe((data: any) => {
          if (data == Status.OK) {
            this.dialogRef.close(true);
            this.clearPath();
            this.notificationsService.showNotification('Wallet request deleted successfully', 'success');
          } else {
            this.toggle();
            this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
          }
        });
      }
    })
  }

  private clearPath(): void {
    let result = this.router.url.split(";");
    this.router.navigateByUrl(result[0]);
  }

  close() {
    if (this.walletRequest.status != this.walletRequestStatus) {
      this.sweetAlertService.showConfirmationDialog().fire({
        html: '<b>Are you sure you want to exit?</b><br><br>Any changes made will be lost!',
      }).then((result) => {
        if (result.isConfirmed) {
          this.walletRequestStatus = this.walletRequest.status;
          this.dialogRef.close();
          this.clearPath();
        }
      })
    } else {
      this.dialogRef.close();
      this.clearPath();
    }
  }
}
