import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { Location } from '@angular/common';

import { WalletService } from '../wallet.service';
import { EmployeesService } from 'src/app/employees/employees.service';
import { PermissionsService } from '@services/permissions.service';
import { NotificationsService } from '@services/notifications.service';

import { WalletRequestComponent } from '../wallet-request/wallet-request.component';
import { WalletRequestDetailsComponent } from '../wallet-request-details/wallet-request-details.component';

import { Employee } from '@models/employee';
import { WalletRequest } from '@models/wallet-request';

import { environment } from '@environments/environment';
import { RequestType } from '@enums/request-type';

const EMPLOYEE_KEY = environment.employee_key;

@Component({
  selector: 'app-wallet-history',
  templateUrl: './wallet-history.component.html',
})
export class WalletHistoryComponent implements OnInit {
  employeeList: Employee[];
  employeeListData: Employee[];
  currentEmployee: Employee;
  walletHistory: WalletRequest[];
  walletHistoryData: WalletRequest[];
  title: string = "Wallet History";
  reviewerName: string;
  status: string;
  employeesHistory: boolean = false;
  requestType = RequestType;

  constructor(
    private router: Router,
    private dialog: MatDialog,
    private permissionsService: PermissionsService,
    private employeesService: EmployeesService,
    private notificationsService: NotificationsService,
    private walletService: WalletService,
    private route: ActivatedRoute,
    private location: Location
  ) {
    this.currentEmployee = JSON.parse(localStorage.getItem(EMPLOYEE_KEY));
  }

  currentEmployeeIsAdmin() {
    return this.permissionsService.currentEmployeeIsAdmin();
  }

  isCrafter() {
    return this.permissionsService.currentEmployeeIsCrafter();
  }

  getEmployee() {
    let employee: Employee;
    this.route.data.subscribe((data: { employee: Employee }) => {
      employee = data.employee;
    })

    return employee;
  }

  goToWalletRequest(type: RequestType) {
    if (type == this.requestType.DEBIT && this.currentEmployee.my_way_points_available == 0) {
      this.notificationsService.showNotification('You don\'t have points.', 'warning');
    } else {
      const dialogRef = this.dialog.open(WalletRequestComponent, {
        width: '50%',
        data: { request_type: type },
        disableClose: true,
        panelClass: 'mobile-dialog',
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.currentEmployee = JSON.parse(localStorage.getItem(EMPLOYEE_KEY));
          this.getWalletHistory();
        }
      });
    }
  }

  goToWalletSummary() {
    this.router.navigate(['../../../', 'wallet', 'summary']);
  }

  getWalletHistory() {
    let employee: Employee = this.getEmployee();
    this.fillWalletList(employee);
  }

  getUserWalletHistory(employeeId: number, title: string) {
    this.walletHistoryData = null;
    this.walletService.getUserWalletHistory(employeeId).subscribe((data: any) => {
      this.walletHistoryData = data;
      this.walletHistory = this.walletHistoryData;
      this.title = title;
      this.openDialogFromURL();
    });
  }

  private openDialogFromURL() {
    const walletRequestId: number = Number.parseInt(this.route.snapshot.paramMap.get('point_request'));

    if (walletRequestId) {
      this.walletRequestDetails(walletRequestId)
    }
  }

  compareUrl() {
    this.router.events.subscribe((data) => {
      if (data instanceof NavigationEnd) {
        this.openDialogFromURL();
      }
    })
  }

  private fillWalletList(employee: Employee) {
    if (!employee) {
      this.getUserWalletHistory(this.currentEmployee.id, `Wallet History`);
    } else {
      if (employee.id == this.currentEmployee.id) {
        this.getUserWalletHistory(this.currentEmployee.id, `My Wallet History`);
      } else {
        this.getUserWalletHistory(employee.id, `Wallet History of ${employee.first_name}`);
      }
    }
  }

  filter(searchTarget: string) {
    this.walletHistory = (searchTarget.length >= 3) ?
      this.filterListByReviewerAndStatus(searchTarget, this.walletHistoryData) :
      this.walletHistoryData;
  }

  filterByEmployeeName(searchTarget: string) {
    this.employeeList = (searchTarget.length >= 3) ?
      this.filterListByEmployeeName(searchTarget, this.employeeListData) :
      this.employeeListData;
  }

  private filterListByEmployeeName(searchTarget: string, dataList: Employee[]) {
    return dataList.filter((employee: Employee) => {
      return employee.first_name.toLowerCase().includes(searchTarget.toLowerCase()) ||
        employee.last_name.toLowerCase().includes(searchTarget.toLowerCase());
    });
  }

  private filterListByReviewerAndStatus(searchTarget: string, dataList: WalletRequest[]) {
    return dataList.filter((walletRequest: WalletRequest) => {
      if (walletRequest.reviewer_name) {
        return walletRequest.reviewer_name.toLowerCase().includes(searchTarget.toLowerCase()) ||
          walletRequest.status.toLowerCase().includes(searchTarget.toLowerCase()) ||
          walletRequest.request_type.toLowerCase().includes(searchTarget.toLowerCase());
      } else {
        return walletRequest.status.toLowerCase().includes(searchTarget.toLowerCase()) ||
          walletRequest.request_type.toLowerCase().includes(searchTarget.toLowerCase());
        ;
      }
    });
  }

  private filterByActive(isActive: boolean, employees: Employee[]) {
    return employees.filter((employee) => employee.active == isActive);
  }

  walletRequestDetails(walletRequestId: number) {
    let walletRequestData = this.walletHistory.find(wallet => wallet.id == walletRequestId);
    if (!!(walletRequestData)) {
      const dialogRef = this.dialog.open(WalletRequestDetailsComponent, {
        width: '40%',
        data: {
          walletRequestData: walletRequestData,
        },
        disableClose: true,
        panelClass: 'mobile-dialog',
      });
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.getUserWalletHistory(walletRequestData.employee_id, 'Wallet History');
        }
      });
    } else {
      this.getWalletHistory();
    }
  }

  async ngOnInit(): Promise<void> {
    try {
      if (this.currentEmployeeIsAdmin() && this.location.path().includes('wallet/history')) {
        this.employeesHistory = true;

        // Fetch employees
        const employeesData: any = await this.employeesService.getEmployees().toPromise();
        this.employeeList = this.filterByActive(true, employeesData);
        this.employeeListData = this.employeeList;

        // Fetch user wallet
        const userWalletData: any = await this.walletService.getUserWalletHistory(this.currentEmployee.id).toPromise();
        this.walletHistory = userWalletData;
      } else if (this.location.path().includes('wallet_history') || !this.currentEmployeeIsAdmin()) {
        this.getWalletHistory();
      }

      this.compareUrl();
    } catch (error) {
      console.error(error);
    }
  }
}
