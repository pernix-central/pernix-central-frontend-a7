import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { StorageService } from '@services/storage.service';

import { WalletRequest } from '@models/wallet-request';
import { environment } from '@environments/environment';

import { RequestStatus } from '@enums/request-status';

const AUTH_KEY = environment.auth_token;

@Injectable({
  providedIn: 'root'
})
export class WalletService {
  private apiUrl = environment.apiUrl;
  private storage: Storage;
  private httpOptions: any;

  constructor(
    private http: HttpClient,
    private storageService: StorageService
  ) {
    this.storage = this.storageService.get();
  }

  createWalletRequest(wallet_request: WalletRequest) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const payload = {
      wallet_request: { ...wallet_request }
    }
    const url = `${this.apiUrl}/v1/my_way_points_transactions`;
    return this.http.post(url, payload, this.httpOptions);
  }

  updateWalletRequest(wallet_request: WalletRequest) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const { employee_name, reviewer_name, created_at,
      formatted_created_at, is_marked, ...filteredWalletRequest } = wallet_request;

    const payload = {
      wallet_request: { ...filteredWalletRequest }
    }
    const url = `${this.apiUrl}/v1/my_way_points_transactions/${wallet_request.id}`;

    return this.http.put(url, payload, this.httpOptions);
  }

  updateStatusWalletRequest(wallet_request_ids: number[], status: RequestStatus) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }

    const payload = {
      ids: wallet_request_ids,
      status: status,
    }
    const url = `${this.apiUrl}/v1/my_way_points_transactions/update_wallets_status`;
    return this.http.put(url, payload, this.httpOptions);
  }

  getUserWalletHistory(employee_id: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/my_way_points_transactions/user_wallet?employee_id=${employee_id}`;
    return this.http.get<WalletRequest[]>(url, this.httpOptions);
  }

  getWalletSummary() {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/my_way_points_transactions/wallet_summary`;
    return this.http.get<WalletRequest[]>(url, this.httpOptions);
  }

  deleteWalletRequest(id: number) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/my_way_points_transactions/${id}`
    return this.http.delete(url, this.httpOptions)
  }

  acceptOrDenyThroughEmail(params: any) {
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': this.storage.getItem(AUTH_KEY)
      })
    }
    const url = `${this.apiUrl}/v1/my_way_points_transactions/${params['request_id']}/update_through_email`
    return this.http.put(url, params['options'] || {}, this.httpOptions);
  }

}
