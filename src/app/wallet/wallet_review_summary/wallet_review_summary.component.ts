import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
import { Sort } from '@angular/material/sort';

import { WalletService } from '../wallet.service';
import { NotificationsService } from '@services/notifications.service';
import { SweetAlertService } from '@services/sweet-alert.service';

import { Employee } from '@models/employee';
import { WalletRequest } from '@models/wallet-request';

import { environment } from '@environments/environment';
import { RequestStatus } from '@enums/request-status';

import Swal from 'sweetalert2';

const EMPLOYEE_KEY = environment.employee_key;

@Component({
  selector: 'app-wallet_review_summary',
  templateUrl: './wallet_review_summary.component.html',
})
export class WalletReviewSummaryComponent implements OnInit {
  currentEmployee: Employee;
  walletSummary: WalletRequest[] = [];
  walletSummaryData: WalletRequest[] = [];
  status = RequestStatus;
  selectedRequests: number[] = [];
  loading: boolean;
  searchTarget: string = '';
  markAll: boolean;

  constructor(
    private router: Router,
    private notificationsService: NotificationsService,
    private walletService: WalletService,
    private route: ActivatedRoute,
    private sweetAlertService: SweetAlertService,
  ) {
    this.currentEmployee = JSON.parse(localStorage.getItem(EMPLOYEE_KEY));
  }

  filter(target: string) {
    this.searchTarget = target;
    this.markAll = false;
    this.walletSummary = this.searchTarget.length >= 3 ?
      (this.resetSelected(), this.filterListByEmployeeNameOrActivityOrDate()) :
      this.walletSummaryData;
  }

  private filterListByEmployeeNameOrActivityOrDate() {
    return this.walletSummaryData.filter((wallet: WalletRequest) => {
      return wallet.employee_name.toLowerCase().includes(this.searchTarget.toLowerCase()) ||
        wallet.request_name.toLowerCase().includes(this.searchTarget.toLowerCase()) ||
        wallet.formatted_created_at.toLowerCase().includes(this.searchTarget.toLowerCase());
    });
  }

  sortWalletSummary(sort: Sort) {
    this.walletSummary = this.sortData(sort)
  }

  private sortData(sort: Sort) {
    const data = this.walletSummary.slice();
    if (!sort.active || sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = sort.direction === 'asc';
      switch (sort.active) {
        case 'date': return this.compare(a.formatted_created_at, b.formatted_created_at, isAsc);
        case 'employee': return this.compare(a.employee_name, b.employee_name, isAsc);
        case 'activity': return this.compare(a.request_name, b.request_name, isAsc);
        case 'points': return this.compare(a.request_points, b.request_points, isAsc);
        default: return 0;
      }
    });
  }

  markRequest(event: any, walletRequest: WalletRequest) {
    this.markAll = false;
    const checkboxIsChecked = event.target.checked;

    if (checkboxIsChecked) {
      walletRequest.is_marked = true;
      this.selectedRequests.push(walletRequest.id);
    } else {
      walletRequest.is_marked = false;
      this.selectedRequests = this.findAndRemoveId(walletRequest);
    }
  }

  markAllRequests(event: any) {
    const checkboxIsChecked = event.target.checked;

    if (checkboxIsChecked) {
      this.markAll = true;
      this.selectedRequests = this.walletSummary.map(request => request.id);
      this.walletSummary = this.walletSummary.map(request => ({
        ...request,
        is_marked: true
      }));
    } else {
      this.resetSelected();
    }
  }

  findAndRemoveId(walletRequest: WalletRequest) {
    return this.selectedRequests.filter(id => id !== walletRequest.id);
  }

  resetSelected() {
    this.markAll = false;
    this.selectedRequests = [];
    this.walletSummary = this.walletSummary.map(request => ({
      ...request,
      is_marked: false
    }));
    this.walletSummaryData = this.walletSummaryData.map(request => ({
      ...request,
      is_marked: false
    }))
  }

  compare(a: string | number, b: string | number, isAsc: boolean) {
    return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
  }

  compareUrl() {
    this.router.events.subscribe((data) => {
      if (data instanceof NavigationEnd) {
        this.getWalletSummary();
      }
    })
  }

  private clearPath(): void {
    let result = this.router.url.split("?")
    this.router.navigateByUrl(result[0]);
  }

  clearSearch() {
    this.searchTarget = '';
    this.filter(this.searchTarget);
  }

  async getWalletSummary() {
    this.walletService.getWalletSummary().subscribe((data: any) => {
      this.walletSummaryData = data;
      this.walletSummary = this.walletSummaryData;
      this.loading = false;
      this.searchTarget = '';
      this.markAll = false;
    });
  }

  async acceptOrDenyWalletRequest() {
    const currentRoute = this.route.snapshot;
    const params = {
      request_id: currentRoute.queryParams['request_id'],
      status: currentRoute.queryParams['status'],
    };

    if (params.request_id && params.status) {
      try {
        const options = {
          status: params.status,
          reviewer_id: this.currentEmployee.id,
        }
        const data: any = await this.walletService.acceptOrDenyThroughEmail({ ...params, options }).toPromise();
        if (data.error) {
          this.notificationsService.showNotification(data.error, 'danger');
        } else {
          this.notificationsService.showNotification(data.message, 'success');
          this.clearPath();
        }
      } catch (error) {
        this.notificationsService.showNotification(error, 'danger')
      }
    }
  }

  async updateStatusWalletRequest(wallet_request_ids: number[], status: RequestStatus) {
    try {
      this.loading = true;

      const data: any = await this.walletService.updateStatusWalletRequest(wallet_request_ids, status).toPromise();

      if (data.error) {
        this.notificationsService.showNotification(data.error, 'danger');
        this.loading = false;
      }
      else {
        this.clearPath();
        this.getWalletSummary();
        this.selectedRequests = [];
        this.notificationsService.showNotification(data.message, 'success');
        this.loading = false;
      }
    } catch (error) {
      this.notificationsService.showNotification('Something went wrong, please try again.', 'danger');
      this.loading = false;
    }
  }

  async updateStatusAllWalletRequest(status: RequestStatus) {
    const request_ids = this.walletSummary.map(request => request.id);
    this.updateStatusWalletRequest(request_ids, status);
  }

  update(status: RequestStatus, all: boolean) {
    this.sweetAlertService.showConfirmationDialog().fire({
      html: `<b>Are you sure you want to "${status}" the points?</b>
      <br><br>This action CANNOT be undone${this.searchTarget != '' && this.selectedRequests.length == 0 ?
          ` and will update those matching '${this.searchTarget.toUpperCase()}'` : ''}!`,
    }).then(async (result) => {
      if (result.isConfirmed) {
        all ? await this.updateStatusAllWalletRequest(status) : await this.updateStatusWalletRequest(this.selectedRequests, status)
      }
    })
  }

  async ngOnInit(): Promise<void> {
    this.loading = true;
    await this.acceptOrDenyWalletRequest();
    await this.getWalletSummary();
    this.compareUrl();
  }
}
