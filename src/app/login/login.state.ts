import { Injectable } from "@angular/core";
import { Action, Selector, State, Store, StateContext } from '@ngxs/store';
import { Login, Logout } from './login.action';
import { StorageService } from '@services/storage.service';
import { environment } from '@environments/environment';

export interface LoginStateModel {
  isLoggedIn: boolean;
}

const AUTH_KEY = environment.auth_token;
const EMPLOYEE_KEY = environment.employee_key;
const NAV_TITLE_KEY = environment.nav_title_key;
export const LOGIN_STATUS_KEY = 'isLoggedIn';
export const TABS_OPEN = 'tabsOpen';
const defaults: LoginStateModel = {
  isLoggedIn: false,
};

@State<LoginStateModel>({
  name: 'LoginState',
  defaults: defaults,
})

@Injectable()
export class LoginState {
  private storage: Storage;

  constructor(private storageService: StorageService,) {
    this.storage = this.storageService.get();
  }
  @Selector()
  static isLoggedIn(state: LoginStateModel) {
    return state.isLoggedIn;
  }

  @Action(Login)
  Login(ctx: StateContext<LoginStateModel>, action: Login) {
    const payload = action.payload;
    ctx.patchState({
      isLoggedIn: true,
    });

    this.storage.setItem(AUTH_KEY, payload.auth_token);
    this.storage.setItem(EMPLOYEE_KEY, JSON.stringify(payload.employee));
    this.storage.setItem(NAV_TITLE_KEY, payload.nav_title);
    this.storage.setItem(LOGIN_STATUS_KEY, 'true');
  }

  @Action(Logout)
  Logout({ getState, patchState }: any) {
    patchState({
      isLoggedIn: false,
    });

    localStorage.clear();
    this.storage.setItem(LOGIN_STATUS_KEY, 'false');
  }
}
