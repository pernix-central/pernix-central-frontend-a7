export class Login {
  static readonly type = '[Login] Login Status';
  constructor(public payload: any) { }
}

export class Logout {
  static readonly type = '[Logout] Logout Status';
}
