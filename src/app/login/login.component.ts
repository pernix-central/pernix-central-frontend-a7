import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';

import { LoginService } from './login.service';
import { StorageService } from '@services/storage.service';
import { NotificationsService } from '@services/notifications.service';

import { MatDialog } from '@angular/material/dialog';
import { PasswordRecoveryComponent } from '../password-recovery/password-recovery.component';

import { Login, Logout } from './login.action';
import { delay } from 'rxjs/operators';
import { Store } from '@ngxs/store';
import { Observable, fromEvent } from 'rxjs';
import { LOGIN_STATUS_KEY, TABS_OPEN } from './login.state';

@Component({
  templateUrl: './login.component.html'
})
export class LoginComponent implements OnInit {

  credentials: any;
  private storage: Storage;
  loading: boolean;
  source$: Observable<Event>;

  constructor(
    private loginService: LoginService,
    private storageService: StorageService,
    private router: Router,
    private dialog: MatDialog,
    private route: ActivatedRoute,
    private notificationsService: NotificationsService,
    private store: Store,
  ) {
    this.storage = this.storageService.get();
    this.credentials = {
      email: '',
      password: ''
    }
  }

  onSubmit() {
    this.loading = true;
    this.credentials.email = this.credentials.email.toLowerCase();
    this.loginService.login(this.credentials).subscribe((data: any) => {
      this.store.dispatch(new Login({ ...data, nav_title: 'Crafting the Future!' }));
      this.goToNextPage();
      this.loading = false;
    }, (error: any) => {
      if (error.error.message == 'Invalid credentials') {
        this.notificationsService.showNotification(`${error.error.message}. Password or email incorrect, please try again`, 'danger');
      } else if (error.error.message == 'Unauthorized request') {
        this.notificationsService.showNotification(`${error.error.message}. Your user is inactive, please contact the administrator.`, 'danger');
      } else {
        this.notificationsService.showNotification(`Please try again later!`, 'danger');
      }
      this.loading = false;
    })
  }

  openPasswordRecoveryModal(): void {
    this.dialog.open(PasswordRecoveryComponent, {
      width: '40%',
    });
  }

  private goToNextPage() {
    const nextPage = this.storage.getItem('URL');

    if (nextPage) {
      const currentRoute = this.route.snapshot.routeConfig?.path;
      const targetRoute = this.router.parseUrl(nextPage)
        .root.children.primary.segments.map(segment => segment.path).join('/');

      this.router.navigateByUrl(currentRoute !== targetRoute ? nextPage : 'home');
    } else {
      this.router.navigate(['home']);
    }
  }

  goToPageBasedOnLoginStatus() {
    if (this.storage.getItem(LOGIN_STATUS_KEY) === "true") {
      this.goToNextPage();
    } else {
      this.router.navigate(['login']);
    }
  }

  logoutIfAllTabsAreClosed() {
    const tabsOpen = localStorage.getItem(TABS_OPEN);
    if (tabsOpen == null) {
      localStorage.setItem(TABS_OPEN, '1');
    } else {
      localStorage.setItem(TABS_OPEN, (parseInt(tabsOpen) + 1).toString());
    }

    window.addEventListener('beforeunload', () => {
      const newTabCount = localStorage.getItem(TABS_OPEN);

      if (newTabCount !== null) {
        const updatedTabCount = parseInt(newTabCount) - 1;
        localStorage.setItem(TABS_OPEN, updatedTabCount.toString());

        if (updatedTabCount === 0) {
          this.store.dispatch(new Logout());
        }
      }
    });
  }

  checkIfLoginStatusChanges() {
    this.source$ = fromEvent(window, 'storage');
    this.source$.pipe(delay(500)).subscribe(
      ldata => {
        this.goToPageBasedOnLoginStatus();
      }, error => {
        console.error('Error in storage event:', error);
      }
    );
  }

  ngOnInit() {
    this.loading = false;
    this.goToPageBasedOnLoginStatus();
    //this.logoutIfAllTabsAreClosed(); //TODO we have to fix a bug with this if we want to activate it
    this.checkIfLoginStatusChanges();
  }
}
