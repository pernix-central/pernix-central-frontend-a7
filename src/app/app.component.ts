import { Component } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html'
})
export class AppComponent {
  title = 'pc-fe-angular6';
  collapse: boolean = true;

  constructor(private router: Router) { }

  getCurrentUrl(): string {
    return this.router.url;
  }

  display(): boolean {
    let url = this.router.url;
    if (url) {
      return (!(this.isLogin() || url.includes('/reset-password')) && url != '/');
    } else {
      return false;
    }
  }

  collapseSidebar(isCollapsed: boolean) {
    this.collapse = isCollapsed;
  }

  isLogin(): boolean {
    return this.router.url === '/login';
  }
}
