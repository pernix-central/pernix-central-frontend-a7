export enum RequestStatus {
    Accepted = "accepted",
    Approved = "approved",
    Denied = "denied",
    Cancelled = "cancelled",
    Pending = "pending",
    Accept = "accept",
    Approve = "approve",
    Cancel = "cancel",
    Deny = "deny"
}