export enum RequestType {
    CREDIT = "credit",
    DEBIT = "debit",
}