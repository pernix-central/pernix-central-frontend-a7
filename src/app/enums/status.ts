export enum Status {
    OK = 'ok',
    NO_CONTENT = 'no_content',
    ALREADY_TAKEN = 'has already been taken',
}