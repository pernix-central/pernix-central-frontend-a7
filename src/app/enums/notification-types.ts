export enum NotificationTypes {
    ANNIVERSARY = 'anniversary',
    MYANNIVERSARY = 'my_anniversary',
    BIRTHDAY = 'birthday',
    MYBIRTHDAY = 'my_birthday',
    RECURRENT = 'recurrent',
    VACATIONSUBMISSION = 'vacation_request_submission',
    VACATIONREVISION = 'vacation_request_revision',
    VACATIONREMINDER = 'vacation_request_reminder',
    ADMINVACATIONREVISION = 'admin_vacation_request_revision',
    ROLECHANGE = 'role_change',
    SUGGESTIONRESPONSES = 'suggestion_responses',
    SCHEDULECHANGE = "change_schedule",
    POINTREDEMPTIONREQUESTSUBMISSION = "wallet_point_redemption_request_submission",
    POINTREQUESTREVISION = "wallet_point_request_revision",
    POINTREDEMPTIONREQUESTREVISION = "wallet_point_redemption_request_revision",
    POINTREQUESTREMINDER = "wallet_request_reminder", 
    ADMINPOINTREQUESTREVISION = "admin_wallet_request_revision"
}