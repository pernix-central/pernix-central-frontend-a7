export enum StatusTypes {
    Accepted = "accepted",
    Denied = "denied",
    Pending = "pending",
    Cancelled = "cancelled",
    Accept = "accept",
    Deny = "deny"
}