// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --configuration production` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  apiUrl: 'http://localhost:3000',

  // storage keys
  auth_token: 'current_employee_token',
  employee_key: 'current_employee',
  nav_title_key: 'nav_title',

  // roles
  roles: [
    { name: 'Admin', tier: 0 },
    { name: 'Crafter', tier: 1 },
    { name: 'Apprentice', tier: 2 },
  ]
};

/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
